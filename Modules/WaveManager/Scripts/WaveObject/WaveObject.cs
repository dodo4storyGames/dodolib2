using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveObject : MonoBehaviour {

    [SerializeField]
    int points = 1;

    [SerializeField]
    string enemyName = "";

    void Start()
    {
        //enemyName = gameObject.name;
    }

    public void AddPoints()
    {
        GetComponentInParent<Wave>().AddPoints(points, enemyName);
    }
}
