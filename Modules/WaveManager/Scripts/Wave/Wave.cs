using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class Wave : MonoBehaviour
{

    [SerializeField]
    List<SpawnArea> spawnAreas = new List<SpawnArea>();

    [SerializeField]
    List<WaveEnemyData> waveEnemies = new List<WaveEnemyData>();

    Dictionary<string, int> enemyMaxCounts = new Dictionary<string, int>();
    Dictionary<string, int> enemyActualCounts = new Dictionary<string, int>();

    [SerializeField]
    Transform enemyHolder;

    [SerializeField]
    string fileSource = "";

    [SerializeField]
    float minBreak = 1f;

    [SerializeField]
    float maxBreak = 2f;

    [SerializeField]
    int pointsToComplete = 20;

    int waveCounter = 0;
    int pointsCounter = 0;
    bool spawnFlag = true;
    bool waveRunning = false;

    Randomizer<WaveEnemyData> randomizer = new Randomizer<WaveEnemyData>();
    WavesData wavesData = new WavesData();
    List<WaveEnemyData> disabledObjects = new List<WaveEnemyData>();

    void Start()
    {
        fileSource = Application.streamingAssetsPath + "/testSave.json";
        string jsonString = File.ReadAllText(fileSource);
        wavesData = JsonUtility.FromJson<WavesData>(jsonString);

        //LoadWave();
    }

    void Update()
    {
        if (waveRunning && spawnFlag && pointsCounter < pointsToComplete)
            StartCoroutine(SpawnEnemyCo());

        if (pointsCounter >= pointsToComplete)
        {
            pointsCounter = 0;
            waveCounter++;

            if (waveCounter < wavesData.GetWavesCount())
            {
                LoadWave();
            }
            else
            {
                PauseWave();
            }
        }
    }

    public void StartWave()
    {
        LoadWave();
        waveRunning = true;
    }

    public void PauseWave()
    {
        waveRunning = false;
    }

    public void StopWave()
    {
        waveRunning = false;
        pointsCounter = 0;
        waveCounter = 0;

        enemyActualCounts.Clear();

        EnemyPoolReset[] enemies = GetComponentsInChildren<EnemyPoolReset>();
        for(int i = 0; i < enemies.Length; i++)
        {
            enemies[i].DestroyMe();
        }
    }

    public void AddPoints(int points, string enemyName)
    {
        pointsCounter += points;
        SubCount(enemyName);
        AddDisabledObject(enemyName);
    }

    void SubCount(string name)
    {
        enemyActualCounts[name]--;
    }

    // Po zniszczeniu obiektu kt�ry wyst�powa� max ilo�� na scenie randomizer ustawia jego prawdopodobie�two na wyj�ciowe
    void AddDisabledObject(string name)
    {
        for(int i = 0; i < disabledObjects.Count; i++)
        {
            if(disabledObjects[i].GetName() == name)
            {
                randomizer.AddObject(disabledObjects[i], disabledObjects[i].GetPropability());
                disabledObjects.RemoveAt(i);
            }
        }
    }

    void LoadData()
    {
        SingleWaveData wave = wavesData.GetWave(waveCounter);
        waveEnemies = wave.GetWaveEnemies();
        minBreak = wave.GetMinBreak();
        maxBreak = wave.GetMaxBreak();
        pointsToComplete = wave.GetPointsToComplite();
    }

    void LoadWave()
    {
        //�adowanie pliku
        LoadData();

        randomizer.Clear();

        for (int i = 0; i < waveEnemies.Count; i++)
        {
            randomizer.AddObject(waveEnemies[i], waveEnemies[i].GetPropability());

            if (!enemyMaxCounts.ContainsKey(waveEnemies[i].GetName()))
            {
                enemyMaxCounts.Add(waveEnemies[i].GetName(), waveEnemies[i].GetMaxCount());
            }
            else
            {
                enemyMaxCounts[waveEnemies[i].GetName()] = waveEnemies[i].GetMaxCount();
            }

            if (!enemyActualCounts.ContainsKey(waveEnemies[i].GetName()))
            {
                enemyActualCounts.Add(waveEnemies[i].GetName(), 0);
            }
        }

    }

    void SpawnEnemy()
    {
        WaveEnemyData obj = randomizer.GetRandomObject();
        SpawnArea spawnArea;

        do
        {
            spawnArea = spawnAreas[Random.Range(0, spawnAreas.Count)];
        } while (obj.GetSpawnAreaTag() != spawnArea.GetSpawnPointTag());

        if (enemyActualCounts[obj.GetName()] < enemyMaxCounts[obj.GetName()])
        {
            ObjectPoolManager.Instance.SpawnPoolObject(obj.GetName(), spawnArea.GetPosition(), enemyHolder);
            enemyActualCounts[obj.GetName()]++;

            // Ustawia prawdopodobie�stwo obiektu kt�ry wyst�puje ju� max ilo�� na scenie na 0 aby randomizer go nie losowa�
            if (enemyActualCounts[obj.GetName()] >= enemyMaxCounts[obj.GetName()])
            {
                disabledObjects.Add(obj);
                randomizer.SetPropability(obj, 0f);
            }
        }
    }

    IEnumerator SpawnEnemyCo()
    {
        spawnFlag = false;
        SpawnEnemy();
        yield return new WaitForSeconds(Random.Range(minBreak, maxBreak));
        spawnFlag = true;
    }
}
