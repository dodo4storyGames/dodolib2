using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : Singleton<WaveManager>
{

    public enum WaveManagerMode { INIT, RUNNING, PAUSE, STOP, WAIT };
    WaveManagerMode mode = WaveManagerMode.INIT;

    Wave[] waves;

	void Start () {
        waves = GetComponentsInChildren<Wave>();
	}
	
	void Update () {

        switch (mode)
        { 
            case WaveManagerMode.INIT:

                //inicjowanie
                StartWaves();
                mode = WaveManagerMode.RUNNING;

                break;

            case WaveManagerMode.RUNNING:

                //normalny tryb
                

                break;

            case WaveManagerMode.PAUSE:

                //nowi przeciwnicy nie pojawiają się
                

                break;

            case WaveManagerMode.STOP:

                //wszyscy wracają do puli, wavy resetowane
                
        
                break;

            case WaveManagerMode.WAIT:

                //czeka na zmianę stanu
                break;
        }
	}

    //ustawianie stanów managera
    public void SetMode(WaveManagerMode modeToSet)
    {
        switch (modeToSet)
        {
            case WaveManagerMode.RUNNING:
                StartWaves();
                break;

            case WaveManagerMode.PAUSE:
                PauseWaves();
                break;

            case WaveManagerMode.STOP:
                StopWaves();
                break;
        }

        mode = modeToSet;
    }

    void StartWaves()
    {
        mode = WaveManagerMode.WAIT;

        for(int i = 0; i < waves.Length; i++)
        {
            waves[i].StartWave();
        }
    }

    void PauseWaves()
    {
        mode = WaveManagerMode.WAIT;

        for (int i = 0; i < waves.Length; i++)
        {
            waves[i].PauseWave();
        }
    }

    void StopWaves()
    {
        mode = WaveManagerMode.WAIT;

        for (int i = 0; i < waves.Length; i++)
        {
            waves[i].StopWave();
        }
    }

    public void StartWave(string waveName)
    {
        Transform waveTransform = transform.Find(waveName);
        if (waveTransform)
        {
            Wave wave = waveTransform.GetComponent<Wave>();
            if (wave)
                wave.StartWave();
            else
                Debug.LogWarning("WaveManager: StartWave(): No 'Wave' component in '"+waveName+"'");
        }
        else
            Debug.LogWarning("WaveManager: StartWave(): No child named: '" + waveName + "'");
        
    }

    public void PauseWave(string waveName)
    {
        Transform waveTransform = transform.Find(waveName);
        if (waveTransform)
        {
            Wave wave = waveTransform.GetComponent<Wave>();
            if (wave)
                wave.PauseWave();
            else
                Debug.LogWarning("WaveManager: PauseWave(): No 'Wave' component in '" + waveName + "'");
        }
        else
            Debug.LogWarning("WaveManager: PauseWave(): No child named: '" + waveName + "'");
    }

    public void StopWave(string waveName)
    {
        Transform waveTransform = transform.Find(waveName);
        if (waveTransform)
        {
            Wave wave = waveTransform.GetComponent<Wave>();
            if (wave)
                wave.StopWave();
            else
                Debug.LogWarning("WaveManager: StopWave(): No 'Wave' component in '" + waveName + "'");
        }
        else
            Debug.LogWarning("WaveManager: StopWave(): No child named: '" + waveName + "'");
    }

}
