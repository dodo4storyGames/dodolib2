using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SingleWaveData  {

    public float minBreak;
    public float maxBreak;
    public int pointsToCompleate;
    public List<WaveEnemyData> enemyData = new List<WaveEnemyData>();


    public SingleWaveData()
    { 
        
    }

    public SingleWaveData(float miB, float mxB, int pTC)
    {
        minBreak = miB;
        maxBreak = mxB;
        pointsToCompleate = pTC;
    }

    public void AddEnemyData(WaveEnemyData wED)
    {
        enemyData.Add(wED);
    }

    public float GetMinBreak()
    {
        return minBreak;
    }

    public float GetMaxBreak()
    {
        return maxBreak;
    }

    public int GetPointsToComplite()
    {
        return pointsToCompleate;
    }

    public List<WaveEnemyData> GetWaveEnemies()
    {
        return enemyData;
    }
}
