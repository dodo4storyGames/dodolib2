using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class WavesData {

    [SerializeField]
    List<SingleWaveData> waves = new List<SingleWaveData>();

    public void AddWave(SingleWaveData w)
    {
        waves.Add(w);
    }

    public SingleWaveData GetWave(int i)
    {
        return waves[i];
    }

    public int GetWavesCount()
    {
        return waves.Count;
    }

    public void SaveData()
    {
        string jsonString = JsonUtility.ToJson(this);
        Debug.LogWarning(jsonString);
        File.WriteAllText(Application.streamingAssetsPath + "/wave1.json", jsonString);
    }
}
