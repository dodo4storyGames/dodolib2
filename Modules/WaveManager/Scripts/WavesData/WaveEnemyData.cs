using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveEnemyData  {

    public string name;
    public float propability;
    public int maxCount;
    public string maxAreaTag;

    public WaveEnemyData()
    { 
    
    }

    public WaveEnemyData(string n, float p, int mC, string mAT)
    {
        name = n;
        propability = p;
        maxCount = mC;
        maxAreaTag = mAT;
    }

    public string GetName()
    {
        return name;
    }

    public float GetPropability()
    {
        return propability;
    }

    public int GetMaxCount()
    {
        return maxCount;
    }

    public string GetSpawnAreaTag()
    {
        return maxAreaTag;
    }
}
