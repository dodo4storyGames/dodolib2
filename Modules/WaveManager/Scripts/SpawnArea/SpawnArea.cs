using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnArea : MonoBehaviour {

    [SerializeField]
    string spawnPointTag = "";

    Vector3 minPos;
    Vector3 maxPos;

	void Start () {
        minPos = GetComponent<BoxCollider>().bounds.min;
        maxPos = GetComponent<BoxCollider>().bounds.max;
    }

    //Zwraca punkt w obrebie swojego collidera
    public Vector3 GetPosition()
    {
        return new Vector3(Random.Range(minPos.x, maxPos.x), Random.Range(minPos.y, maxPos.y), Random.Range(minPos.z, maxPos.z));
    }

    public string GetSpawnPointTag()
    {
        return spawnPointTag;
    }
}
