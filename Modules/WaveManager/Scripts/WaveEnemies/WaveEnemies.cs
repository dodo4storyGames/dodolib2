using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveEnemies {

    [SerializeField]
    string enemyName = "";

    [SerializeField]
    float propability = 1f;

    [SerializeField]
    int maxCount = 5;

    [SerializeField]
    string spawnAreaTag = "";

    public string GetName()
    {
        return enemyName;
    }

    public float GetPropability()
    {
        return propability;
    }

    public int GetMaxCount()
    {
        return maxCount;
    }

    public string GetSpawnAreaTag()
    {
        return spawnAreaTag;
    }
}
