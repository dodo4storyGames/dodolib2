using UnityEngine;
using UnityEngine.Events;

// TODO: Screen Properties
public abstract class AUIScreenController : MonoBehaviour
{
    [SerializeField]
    protected int zIndex;
    public bool IsVisible { get; private set; }
    public string ScreenId;

    protected Vector2 initialPosition;

    protected RectTransform rectTransform;

    protected IScreenProperties screenProperties;

    protected virtual void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        initialPosition = rectTransform.position;
    }

    public virtual void Show(IScreenProperties properties = null)
    {
        gameObject.SetActive(true);
        rectTransform.SetSiblingIndex(zIndex);

        if (properties != null)
        {
            screenProperties = properties;
            OnPropertiesSet();
        }

        ATweenInComponent tween = GetComponent<ATweenInComponent>();

        if (tween != null)
        {
            tween.Animate();
        }
    }

    public virtual void Show(UnityAction callback, IScreenProperties properties = null)
    {
        gameObject.SetActive(true);
        rectTransform.SetSiblingIndex(zIndex);

        if (properties != null)
        {
            screenProperties = properties;
            OnPropertiesSet();
        }

        ATweenInComponent tween = GetComponent<ATweenInComponent>();

        if (tween != null)
        {
            tween.Animate(() =>
            {
                AnimateCallback();
               
                if (callback != null)
                {
                    callback();
                }
            });
        } else
        {
            AnimateCallback();
        }
    }

    protected abstract void OnPropertiesSet();

    protected virtual void AnimateCallback()
    {

    }

    public virtual void Hide()
    {
        ATweenOutComponent tween = GetComponent<ATweenOutComponent>();

        if (tween != null && gameObject.activeInHierarchy)
        {
            tween.Animate(() =>
            {
                gameObject.SetActive(false);
            });
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public virtual void Hide(UnityAction callback)
    {
        ATweenOutComponent tween = GetComponent<ATweenOutComponent>();

        if (tween != null && gameObject.activeInHierarchy)
        {
            tween.Animate(() =>
            {
                gameObject.SetActive(false);
                callback();
            });
        } else
        {
            gameObject.SetActive(false);
            callback();
        }
    }
}
