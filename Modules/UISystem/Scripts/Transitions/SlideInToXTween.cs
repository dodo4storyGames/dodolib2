using UnityEngine;
using DG.Tweening;

public class SlideInToXTween : ATweenInComponent {
    [SerializeField]
    private float to;
    [SerializeField]
    private float duration;
    [SerializeField]
    private bool targetRectTransform;

    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public override void Animate()
    {
        if (targetRectTransform)
        {
            rectTransform.DOAnchorPosX(to, duration);
        }
        else
        {
            transform.DOMoveX(to, duration);
        }
    }

    public override void Animate(DG.Tweening.TweenCallback callback)
    {
        if (targetRectTransform)
        {
            rectTransform.DOAnchorPosX(to, duration).OnComplete(callback);
        }
        else
        {
            transform.DOMoveX(to, duration).OnComplete(callback);
        }
    }
}
