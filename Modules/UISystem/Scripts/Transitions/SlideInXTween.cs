using UnityEngine;
using DG.Tweening;

public class SlideInXTween : ATweenInComponent {
    [SerializeField]
    private float from;
    [SerializeField]
    private float duration;
    [SerializeField]
    private bool targetRectTransform;

    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public override void Animate()
    {
        if (targetRectTransform)
        {
            rectTransform.DOAnchorPosX(from, duration).From();
        } else
        {
            transform.DOMoveX(from, duration).From();
        }
    }

    public override void Animate(DG.Tweening.TweenCallback callback)
    {
        if (targetRectTransform)
        {
            rectTransform.DOAnchorPosX(from, duration).From().OnComplete(callback);
        }
        else
        {
            transform.DOMoveX(from, duration).From().OnComplete(callback);
        }
    }
}
