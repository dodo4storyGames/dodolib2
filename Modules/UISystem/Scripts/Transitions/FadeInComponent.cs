using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class FadeInComponent : ATweenInComponent {
    [SerializeField]
    private Image image;
    [SerializeField]
    private TextMeshProUGUI text;
    [SerializeField]
    private float fromAlpha;
    [SerializeField]
    private float duration;

    private void Awake()
    {
        if (image == null)
        {
            image = GetComponent<Image>();
        }
    }

    public override void Animate()
    {
        image.DOFade(fromAlpha, duration).From();

        if (text != null)
        {
            text.DOFade(fromAlpha, duration).From();
        }
    }

    public override void Animate(DG.Tweening.TweenCallback callback)
    {
        image.DOFade(fromAlpha, duration).From().OnComplete(callback);

        if (text != null)
        {
            text.DOFade(fromAlpha, duration).From();
        }
    }
}
