using UnityEngine;
using DG.Tweening;

public abstract class ATweenOutComponent : MonoBehaviour
{
    public abstract void Animate();

    public abstract void Animate(DG.Tweening.TweenCallback callback);
}
