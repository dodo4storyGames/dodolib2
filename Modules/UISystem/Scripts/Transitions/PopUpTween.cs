using DG.Tweening;
using UnityEngine;

public class PopUpTween : ATweenInComponent {
    [SerializeField]
    private Vector3 initialScale;
    [SerializeField]
    private Vector3 targetScale;
    [SerializeField]
    private Vector3 maxScale;
    [SerializeField]
    private float duration;
    [SerializeField]
    private float waitAfterTime;
    [SerializeField]
    private float delay;

    public override void Animate()
    {
        DoAnimate();
    }

    public override void Animate(TweenCallback callback)
    {
        DoAnimate(callback);
    }

    private void DoAnimate(TweenCallback callback = null)
    {
        transform.localScale = initialScale;
        Sequence seq = DOTween.Sequence();
        seq
            .SetDelay(delay)
            .Append(transform.DOScale(targetScale, duration / 2));
            

        if (maxScale != Vector3.zero)
        {
            seq
                .Append(transform.DOScale(maxScale, duration / 4))
                .Append(transform.DOScale(targetScale, duration / 4));
        }

        seq.Append(transform.DOScale(targetScale, waitAfterTime));

        if (callback != null)
        {
            seq.OnComplete(callback);
        }

        seq.Play();
    }
}
