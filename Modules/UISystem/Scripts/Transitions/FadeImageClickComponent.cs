using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeImageClickComponent : ATweenClickComponent {
    [SerializeField]
    private float toAlpha;
    [SerializeField]
    private float duration;

    public override void Animate()
    {
        Transform highlightImageContainer = transform.Find("Highlight");
        Image highlightImage = highlightImageContainer != null ? highlightImageContainer.GetComponent<Image>() : null;

        if (highlightImage)
        {
            //highlightImage.DOFade(toAlpha, duration);
            Sequence clickSequence = DOTween.Sequence();
            clickSequence
                .Append(highlightImage.DOFade(toAlpha, duration))
                .Append(highlightImage.DOFade(0, duration));

            clickSequence.Play();
        }
    }

    public override void Animate(DG.Tweening.TweenCallback callback)
    {
        Transform highlightImageContainer = transform.Find("Highlight");
        Image highlightImage = highlightImageContainer != null ? highlightImageContainer.GetComponent<Image>() : null;

        if (highlightImage)
        {
            //highlightImage.DOFade(toAlpha, duration).OnComplete(callback);
            Sequence clickSequence = DOTween.Sequence();
            clickSequence
                .Append(highlightImage.DOFade(toAlpha, duration))
                .Append(highlightImage.DOFade(0, duration));

            clickSequence.Play().OnComplete(callback);
        }
    }
}
