using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class FadeOutComponent : ATweenOutComponent {
    [SerializeField]
    private Image image;
    [SerializeField]
    private TextMeshProUGUI text;
    private float toAlpha;
    [SerializeField]
    private float duration;

    private void Awake()
    {
        if (image == null)
        {
            image = GetComponent<Image>();
        }
    }

    public override void Animate()
    {
        image.DOFade(toAlpha, duration);

        if (text != null)
        {
            text.DOFade(toAlpha, duration);
        }
    }

    public override void Animate(DG.Tweening.TweenCallback callback)
    {
        image.DOFade(toAlpha, duration).OnComplete(callback);

        if (text != null)
        {
            text.DOFade(toAlpha, duration);
        }
    }
}
