using System.Collections;
using UnityEngine;
using DG.Tweening;

public class AnimateInChildrenWidgetsTween : ATweenInComponent {
    [SerializeField]
    private float timeBetweenChildren;

    public override void Animate()
    {
        StartCoroutine(RunAnimationSequence());
    }

    public override void Animate(TweenCallback callback)
    {
        StartCoroutine(RunAnimationSequence(callback));
    }

    private IEnumerator RunAnimationSequence()
    {
        AUIAnimatedWidgetController[] widgets = GetComponentsInChildren<AUIAnimatedWidgetController>();

        yield return new WaitForEndOfFrame();

        foreach (AUIAnimatedWidgetController widget in widgets)
        {
            widget.AnimateIn();

            yield return new WaitForSeconds(timeBetweenChildren);
        }

        yield return null;
    }

    private IEnumerator RunAnimationSequence(DG.Tweening.TweenCallback callback)
    {
        AUIAnimatedWidgetController[] widgets = GetComponentsInChildren<AUIAnimatedWidgetController>();

        yield return new WaitForEndOfFrame();

        foreach (AUIAnimatedWidgetController widget in widgets)
        {
            widget.AnimateIn();

            yield return new WaitForSeconds(timeBetweenChildren);
        }

        callback(); // TODO: this callback should wait for widget.AnimateIn() to finish.

        yield return null;
    }
}
