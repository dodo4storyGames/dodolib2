using DG.Tweening;
using UnityEngine;
using TMPro;

public class TextMeshFontSizeChangeTween : ATweenClickComponent
{
    [SerializeField]
    private TextMeshProUGUI textMesh;
    [SerializeField]
    private float targetFontSize;
    [SerializeField]
    private float duration;
    private float initialFontSize;

    private void Awake()
    {
        initialFontSize = textMesh.fontSize;
    }

    public override void Animate()
    {
        DoAnimate();
    }

    public override void Animate(TweenCallback callback)
    {
        DoAnimate(callback);
    }

    private void DoAnimate(TweenCallback callback = null)
    {
        textMesh.enableAutoSizing = false;
        Sequence seq = DOTween.Sequence();
        seq
            .Append(DOTween.To(() => textMesh.fontSize, x => textMesh.fontSize = x, targetFontSize, duration / 2))
            .Append(DOTween.To(() => textMesh.fontSize, x => textMesh.fontSize = x, initialFontSize, duration / 2))
            .OnComplete(() =>
            {
                if (callback != null)
                {
                    callback();
                }

                textMesh.enableAutoSizing = true;
            });

        seq.Play();
    }
}
