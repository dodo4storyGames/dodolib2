using UnityEngine;
using DG.Tweening;

public abstract class ATweenInComponent : MonoBehaviour {
    public abstract void Animate();

    public abstract void Animate(DG.Tweening.TweenCallback callback);
}
