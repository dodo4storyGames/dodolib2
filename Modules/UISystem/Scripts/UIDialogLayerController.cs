using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIDialogLayerController : MonoBehaviour {
    protected Dictionary<string, UIDialogScreenController> screenControllers = new Dictionary<string, UIDialogScreenController>();

    public void ShowScreen(UIDialogScreenController dialogScreen, IScreenProperties properties = null)
    {
        if (!screenControllers.ContainsKey(dialogScreen.ScreenId))
        {
            UIDialogScreenController dialogInstance = Instantiate(dialogScreen, transform);
            screenControllers.Add(dialogScreen.ScreenId, dialogInstance);
        }

        screenControllers[dialogScreen.ScreenId].Show(properties);
    }

    public void ShowScreen(UIDialogScreenController dialogScreen, IScreenProperties properties = null, UnityAction callback = null)
    {
        if (!screenControllers.ContainsKey(dialogScreen.ScreenId))
        {
            UIDialogScreenController dialogInstance = Instantiate(dialogScreen, transform);
            screenControllers.Add(dialogScreen.ScreenId, dialogInstance);
        }

        screenControllers[dialogScreen.ScreenId].Show(callback, properties);
    }

    public void HideScreen(UIDialogScreenController dialogScreen)
    {
        if (screenControllers.ContainsKey(dialogScreen.ScreenId))
        {
            screenControllers[dialogScreen.ScreenId].Hide();
        }
    }

    public void HideScreen(string dialogId)
    {
        if (screenControllers.ContainsKey(dialogId))
        {
            screenControllers[dialogId].Hide();
        }
    }

    public void HideScreen(string dialogId, UnityAction callback)
    {
        if (screenControllers.ContainsKey(dialogId))
        {
            screenControllers[dialogId].Hide(callback);
        }
    }

    public void HideAll(UnityAction callback)
    {
        StartCoroutine(HideAllDialogs(callback));
    }

    public IEnumerator HideAllDialogs(UnityAction callback)
    {
        int callbackStack = 0;

        foreach (KeyValuePair<string, UIDialogScreenController> entry in screenControllers)
        {
            callbackStack++;
            entry.Value.Hide(() =>
            {
                callbackStack--;
            });
        }

        // Wait for all Hide() methods to finish.
        while (callbackStack > 0)
        {
            yield return null;
        }

        callback();

        yield return null;
    }

    public bool HasScreen(string screenId)
    {
        if (screenControllers.ContainsKey(screenId))
        {
            return true;
        }

        return false;
    }

    public UIDialogScreenController GetScreen(string screenId)
    {
        if (screenControllers.ContainsKey(screenId))
        {
            return screenControllers[screenId];
        }

        return null;
    }
}
