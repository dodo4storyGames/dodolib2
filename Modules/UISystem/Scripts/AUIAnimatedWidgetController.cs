using UnityEngine;
using UnityEngine.UI;

public abstract class AUIAnimatedWidgetController : MonoBehaviour {
    [SerializeField]
    protected Image defaultImage;
    [SerializeField]
    protected Image highlightImage;
    [SerializeField]
    protected Image activeImage;

    protected virtual void Start()
    {
        
    }

    public virtual void AnimateIn(DG.Tweening.TweenCallback callback = null)
    {
        gameObject.SetActive(true);
        ATweenInComponent tween = GetComponent<ATweenInComponent>();

        if (tween != null)
        {
            tween.Animate(callback);
        }
    }

    public virtual void AnimateOut(DG.Tweening.TweenCallback callback = null)
    {
        ATweenOutComponent tween = GetComponent<ATweenOutComponent>();

        if (tween != null)
        {
            tween.Animate(callback);
        }
    }

    public virtual void AnimateClick(DG.Tweening.TweenCallback callback)
    {
        ATweenClickComponent tween = GetComponent<ATweenClickComponent>();

        if (tween != null)
        {
            tween.Animate(callback);
        }

    }

    protected Vector3 initialPosition;

    protected virtual void Awake()
    {
        initialPosition = GetComponent<RectTransform>().position;
    }
}
