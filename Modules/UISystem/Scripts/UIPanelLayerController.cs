using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIPanelLayerController : MonoBehaviour
{
    protected Dictionary<string, UIPanelScreenController> screenControllers = new Dictionary<string, UIPanelScreenController>();

    public void ShowScreen(UIPanelScreenController panelScreen, IScreenProperties properties = null)
    {
        if (!screenControllers.ContainsKey(panelScreen.ScreenId))
        {
            UIPanelScreenController panelInstance = Instantiate(panelScreen, transform);
            screenControllers.Add(panelScreen.ScreenId, panelInstance);
        }

        screenControllers[panelScreen.ScreenId].Show(properties);
    }

    public void ShowScreen(UIPanelScreenController panelScreen, UnityAction callback, IScreenProperties properties = null)
    {
        if (!screenControllers.ContainsKey(panelScreen.ScreenId))
        {
            UIPanelScreenController panelInstance = Instantiate(panelScreen, transform);
            screenControllers.Add(panelScreen.ScreenId, panelInstance);
        }

        screenControllers[panelScreen.ScreenId].Show(callback, properties);
    }

    public void HideScreen(UIPanelScreenController panelScreen)
    {
        if (screenControllers.ContainsKey(panelScreen.ScreenId))
        {
            screenControllers[panelScreen.ScreenId].Hide();
        }
    }

    public void HideScreen(string panelId)
    {
        if (screenControllers.ContainsKey(panelId))
        {
            screenControllers[panelId].Hide();
        }
    }

    public void HideScreen(string panelId, UnityAction callback)
    {
        if (screenControllers.ContainsKey(panelId))
        {
            screenControllers[panelId].Hide(callback);
        }
    }

    public void HideAll(UnityAction callback)
    {
        StartCoroutine(HideAllPanels(callback));
    }

    public IEnumerator HideAllPanels(UnityAction callback)
    {
        int callbackStack = 0;

        foreach (KeyValuePair<string, UIPanelScreenController> entry in screenControllers)
        {
            callbackStack++;
            entry.Value.Hide(() =>
            {
                callbackStack--;
            });
        }

        // Wait for all Hide() methods to finish.
        while (callbackStack > 0)
        {
            yield return null;
        }

        callback();

        yield return null;
    }

    public bool HasScreen(string screenId)
    {
        if (screenControllers.ContainsKey(screenId))
        {
            return true;
        }

        return false;
    }

    public UIPanelScreenController GetScreen(string screenId)
    {
        if (screenControllers.ContainsKey(screenId))
        {
            return screenControllers[screenId];
        }

        return null;
    }
}
