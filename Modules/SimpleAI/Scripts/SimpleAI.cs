using System.Collections;
using UnityEngine;

public class SimpleAI : MonoBehaviour {

    enum AIState { INIT, PATROL, ATTACK, LOST_TARGET, DEAD, STAND_UP};
    AIState state = AIState.INIT;

    GameObject player;
    Transform pathTransformParent;
    Transform head;
    Transform[] waypoints;
    Vector3[] waypointsPos;
    int currentNodeIndex = 0;

    [SerializeField]
    float maxhp;
    float hp;

    [SerializeField]
    float fireFrequency;

    [SerializeField]
    float raycastFrequency = 0.2f;

    [SerializeField]
    float fireDamage;

    [SerializeField]
    float distanceToEngage = 3f;  

    [SerializeField]
    float speed = 2f;

    [SerializeField]
    float rotSpeed = 5f;

    [SerializeField]
    float waypointAcc = 0.1f;

    [SerializeField]
    float fov = 60f;

    Animator anim;
    Rigidbody[] ragdoll;
    bool fireFlag;
    bool raycastFlag;

    void Start () {
        anim = GetComponent<Animator>();
        pathTransformParent = transform.Find("WalkPath");
        head = transform.Find("Root/Ribs/Neck/Head");
        waypoints = pathTransformParent.GetComponentsInChildren<Transform>();
        player = GameObject.FindGameObjectWithTag("Player");
        ragdoll = GetComponentsInChildren<Rigidbody>();

        fireFlag = true;
        raycastFlag = true;

        waypointsPos = new Vector3[waypoints.Length];

        for(int i = 0; i < waypoints.Length; i++)
        {
            waypointsPos[i] = waypoints[i].position;
        }
	}
	
	void Update () {

        DebugSwitchState();

        switch (state)
        { 
            case AIState.INIT:
                Init();
                break;

            case AIState.PATROL:
                Patrol();
                break;

            case AIState.ATTACK:
                Attack();
                break;

            case AIState.LOST_TARGET:
                SetState(AIState.PATROL);
                break;

            case AIState.DEAD:
                Dead();
                break;

            case AIState.STAND_UP:
                StandUp();
                break;

        }

	}

    void SetState(AIState newState)
    {
        state = newState;
        switch (newState)
        {
            case AIState.INIT:

                break;

            case AIState.DEAD:
                Dead();
                break;
        }
    }

    void Init()
    {
        hp = maxhp;
        //currentNodeIndex = 0;

        
        SetState(AIState.PATROL);
        anim.applyRootMotion = false;
    }

    void Patrol()
    {
        anim.SetBool("Walk", true);

        if (waypointsPos.Length > 0)
        {
            if(Vector3.Distance(waypointsPos[currentNodeIndex], transform.position) <= waypointAcc)
            {
                currentNodeIndex++;
                if(currentNodeIndex >= waypointsPos.Length)
                {
                    currentNodeIndex = 0;
                }
            }

            Vector3 direction = waypointsPos[currentNodeIndex] - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);
            transform.Translate(0, 0, speed * Time.deltaTime);
        }

        Vector3 dir = player.transform.position - transform.position;
        dir.y = 0;
        float angle = Vector3.Angle(dir, transform.forward);

        if (Vector3.Distance(player.transform.position, transform.position) < distanceToEngage && angle < fov/2)
        {
            if (raycastFlag)
            {
                raycastFlag = false;
                StartCoroutine(ShootingRay(raycastFrequency));
            }
        }
    }

    void Attack()
    {
        anim.SetBool("Attack", true);

        if (Vector3.Distance(player.transform.position, transform.position) > distanceToEngage)
        {
            SetState(AIState.LOST_TARGET);
            anim.SetBool("Attack", false);
        }

        Vector3 direction = player.transform.position - transform.position;
        direction.y = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);

        if (fireFlag)
        {
            fireFlag = false;
            StartCoroutine(FireCo(fireFrequency));
        }
    }

    void Fire()
    {
        Debug.Log("Fire");
    }

    public void GetHit(float damage)
    {
        hp -= damage;

        if (hp <= 0)
            SetState(AIState.DEAD); 
    }

    void Dead()
    {
        if (!anim.GetBool("Dead"))
        {
            anim.SetBool("Dead", true);
            anim.applyRootMotion = true;
            StartCoroutine(RagdollCo(4f));
        }
    }

    void StandUp()
    {
        SetRagdollActive(false);
        anim.SetBool("Dead", false);
        anim.SetTrigger("Revive");
        
        StartCoroutine(ReviveCo(4f));
    }

    public void SetRagdollActive(bool active)
    {
        if (active)
        {
            anim.enabled = false;

            for( int i = 0; i < ragdoll.Length; i++)
            {
                ragdoll[i].isKinematic = false;
            }
        }
        else
        {
            anim.enabled = true;

            for (int i = 0; i < ragdoll.Length; i++)
            {
                ragdoll[i].isKinematic = true;
            }
        }
    }

    void DebugSwitchState()
    {
        if (Input.GetKeyDown("1")) SetState(AIState.STAND_UP);
        if (Input.GetKeyDown("2")) SetState(AIState.DEAD);
    }

    IEnumerator FireCo(float t)
    {
        yield return new WaitForSeconds(t);
        Fire();
        fireFlag = true;
    }

    IEnumerator RagdollCo(float t)
    {
        yield return new WaitForSeconds(t);
        SetRagdollActive(true);
    }

    IEnumerator ReviveCo(float t)
    {
        yield return new WaitForSeconds(t);
        SetState(AIState.INIT);
    }

    IEnumerator ShootingRay(float t)
    {
        yield return new WaitForSeconds(t);
        RaycastHit hit;
        Vector3 raycastDir = player.transform.position - head.position;
        Ray playerRay = new Ray(head.position, raycastDir);
        Debug.DrawRay(head.position, raycastDir);

        if (Physics.Raycast(playerRay, out hit, distanceToEngage))
        {
            if (hit.collider.tag == "Player")
            {
                SetState(AIState.ATTACK);
                anim.SetBool("Walk", false);
            }
        }

        raycastFlag = true;
    }
}
