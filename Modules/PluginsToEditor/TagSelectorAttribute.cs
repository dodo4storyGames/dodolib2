﻿using UnityEngine;

/// <summary>
/// Rozwijane menu z lista wszystkich tagow.
/// </summary>
public class TagSelectorAttribute : PropertyAttribute
{
    public bool UseDefaultTagFieldDrawer = false;
}