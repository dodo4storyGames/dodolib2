﻿using UnityEngine;

/// <summary>
/// Wartości nie można edytować w inspektorze.
/// </summary>
public class ReadOnlyAttribute : PropertyAttribute
{

}