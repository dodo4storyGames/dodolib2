﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dTween  {

    //Zwraca wygladzoną ścieżkę
	public static Vector3[] GetSmoothPath(Vector3[] orginalPath, float density = 1f)
    {
        if (orginalPath.Length <= 2)
            return orginalPath;

        int pathLenght = Mathf.RoundToInt((float)orginalPath.Length * density);
        Vector3[] returnPath = new Vector3[pathLenght];

        for (int i = 0; i < returnPath.Length; i++)
        {
            returnPath[i] = iTween.PointOnPath(orginalPath, ((float)i / (float)(returnPath.Length-1)));            
        }

        return returnPath;
    }

    //Zwraca wygladzoną ścieżkę
    public static List<Vector3> GetSmoothPath(List<Vector3> orginalPath, float density = 1f)
    {
        Vector3[] tempVector = new Vector3[orginalPath.Count];
        for (int i = 0; i < orginalPath.Count; i++)
        {
            tempVector[i] = orginalPath[i];
        }

        Vector3[] returnVector = GetSmoothPath(tempVector, density);
        List<Vector3> returnList = new List<Vector3>();

        foreach(Vector3 v in returnVector)
        {
            returnList.Add(v);
        }

        return returnList;
    }

}
