﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TerrainType
{
    [Header("Select only one layer!")]
    public LayerMask terrainMask;
    [Tooltip("Value betwen 0 and infinity.")]
    public int terrainPenalty;
}
