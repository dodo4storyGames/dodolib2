﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

public class PlayerTestPath2 : MonoBehaviour
{
    public bool drawPath;
    private Stopwatch sw, sw2;
    public Vector3 startOffset;
    [Range(-0.5f, 0.5f)]
    public float offsetX;
    [Range(-0.5f, 0.5f)]
    public float offsetY;
    [Range(-0.5f, 0.5f)]
    public float offsetZ;

    [Space(10)]
    public LayerMask hitable;
    public float moveSpeed;
    public GameObject unit;
    private bool simplifyPath = false;
    private float currentPathPrecent;
    private Vector3[] path;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                sw = new Stopwatch();
                sw.Start();
                //PathRequestManager.RequestPath(transform.position, hit.point, OnPathFound);
            }
        }
        CheckOffset();
        SetOffset();
    }

    private void OnPathFound(Vector3[] wayPoints, bool pathFound)
    {
        sw.Stop();
        print("Path found " + pathFound + " " + wayPoints.Length + " : " + sw.ElapsedMilliseconds + " ms");
        if (pathFound == true)
        {
            StopAllCoroutines();
            Vector3[] start = new Vector3[1];
            start[0] = transform.position;
            path = CombineVector3Arrays(start, wayPoints);
            StartCoroutine("FollowPath");
        }
    }

    private Vector3[] CombineVector3Arrays(Vector3[] first, Vector3[] second)
    {
        Vector3[] newArray = new Vector3[first.Length + second.Length];
        Array.Copy(first, newArray, first.Length);
        Array.Copy(second, 0, newArray, first.Length, second.Length);
        return newArray;
    }

    private IEnumerator FollowPath()
    {
        currentPathPrecent = (moveSpeed * Time.deltaTime) / iTween.PathLength(path);
        while (true)
        {
            if (currentPathPrecent == 1)
            {
                yield break;
            }
            iTween.PutOnPath(gameObject, path, currentPathPrecent);
            currentPathPrecent += (moveSpeed * Time.deltaTime) / iTween.PathLength(path);
            currentPathPrecent = Mathf.Clamp(currentPathPrecent, 0f, 1f);
            yield return null;
        }
    }

    private void SetOffset()
    {
        unit.transform.position = transform.position + new Vector3(offsetX + startOffset.x, offsetY + startOffset.y, offsetZ + startOffset.z);
        //unit.transform.position = Vector3.Lerp(unit.transform.position, transform.position + new Vector3(offsetX + startOffset.x, offsetY + startOffset.y, offsetZ + startOffset.z), Time.deltaTime * moveSpeed);
    }

    private void CheckOffset()
    {
        Ray rayUp = new Ray(unit.transform.position, Vector3.up);
        Ray rayDown = new Ray(unit.transform.position, Vector3.down);
        Ray rayForward = new Ray(unit.transform.position, Vector3.forward);
        Ray rayBack = new Ray(unit.transform.position, Vector3.back);
        Ray rayLeft = new Ray(unit.transform.position, Vector3.left);
        Ray rayRight = new Ray(unit.transform.position, Vector3.right);
        // skosy na poziome 0
        //Ray ray_110 = new Ray(unit.transform.position, new Vector3(-1, 0, 1));// x++ z--
        //Ray ray110 = new Ray(unit.transform.position, new Vector3(1, 0, 1)); //x-- z--
        //Ray ray1_1 = new Ray(unit.transform.position, new Vector3(1, 0, -1)); // x-- z++
        //Ray ray_1_1 = new Ray(unit.transform.position, new Vector3(-1, 0, -1)); // x++ z++
        //RaycastHit hit;
        //bool rayY = false;
        //bool rayX = false;
        //bool rayZ = false;

        //if (Physics.Raycast(ray_110, out hit, 0.4f, hitable))
        //{
        //    offsetX -= hit.distance - 0.35f;
        //    offsetZ += hit.distance - 0.35f;
        //}

        //if (Physics.Raycast(ray_110, out hit, 0.4f, hitable))
        //{
        //    if (hit.distance <= 0.3f)
        //    {

        //    }
        //}
        //if (Physics.Raycast(ray_110, out hit, 0.4f, hitable))
        //{
        //    if (hit.distance <= 0.3f)
        //    {

        //    }
        //}
        //if (Physics.Raycast(ray_110, out hit, 0.4f, hitable))
        //{
        //    if (hit.distance <= 0.3f)
        //    {

        //    }
        //}

        //if (Physics.Raycast(rayUp, out hit, 0.3f, hitable))
        //{
        //    if (hit.distance <= 0.3f)
        //    {
        //        offsetY += hit.distance - 0.25f;
        //        rayY = true;
        //    }
        //}
        //if (Physics.Raycast(rayDown, out hit, 0.3f, hitable))
        //{
        //    if (hit.distance <= 0.3f)
        //    {
        //        offsetY -= hit.distance - 0.25f;
        //        rayY = true;
        //    }
        //}

        //if (Physics.Raycast(rayForward, out hit, 0.3f, hitable))
        //{
        //    offsetZ += hit.distance - 0.25f;
        //    rayZ = true;
        //}
        //if (Physics.Raycast(rayBack, out hit, 0.3f, hitable))
        //{
        //    offsetZ -= hit.distance - 0.25f;
        //    rayZ = true;
        //}

        //if (Physics.Raycast(rayRight, out hit, 0.3f, hitable))
        //{
        //    offsetX += hit.distance - 0.25f;
        //    rayX = true;
        //}
        //if (Physics.Raycast(rayLeft, out hit, 0.3f, hitable))
        //{
        //    offsetX -= hit.distance - 0.25f;
        //    rayX = true;
        //}

        //if (rayY == false)
        //{
        //    offsetY = Mathf.Lerp(offsetY, 0, Time.deltaTime * moveSpeed);
        //}

        //if(rayX == false)
        //{
        //    offsetX = Mathf.Lerp(offsetX, 0, Time.deltaTime * moveSpeed);
        //}

        //if(rayZ == false)
        //{
        //    offsetZ = Mathf.Lerp(offsetZ, 0, Time.deltaTime * moveSpeed);
        //}

        //offsetX = Mathf.Clamp(offsetX, -0.5f, 0.5f);
        //offsetY = Mathf.Clamp(offsetY, -0.5f, 0.5f);
        //offsetZ = Mathf.Clamp(offsetZ, -0.5f, 0.5f);
    }

    private void OnDrawGizmos()
    {
        if (drawPath && path != null)
        {
            iTween.DrawPath(path, Color.red);
        }
        Gizmos.color = Color.green;
        Gizmos.DrawLine(unit.transform.position, unit.transform.position + new Vector3(0, -1, 0) * 0.3f);
        Gizmos.DrawLine(unit.transform.position, unit.transform.position + new Vector3(0, 1, 0) * 0.3f);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(unit.transform.position, unit.transform.position + new Vector3(0, 0, 1) * 0.3f);
        Gizmos.DrawLine(unit.transform.position, unit.transform.position + new Vector3(0, 0, -1) * 0.3f);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(unit.transform.position, unit.transform.position + new Vector3(1, 0, 0) * 0.3f);
        Gizmos.DrawLine(unit.transform.position, unit.transform.position + new Vector3(-1, 0, 0) * 0.3f);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(unit.transform.position, unit.transform.position + new Vector3(-1, 0, 1) * 0.35f);
        Gizmos.DrawLine(unit.transform.position, unit.transform.position + new Vector3(1, 0, 1) * 0.35f);
    }
}
