﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Diagnostics;

public class GridManager : Singleton<GridManager>
{
    public bool useLevelData = true;
    public GameObject[] grids;

    private Grid gridFloor;
    private Grid gridFloorWallRoof;
    private Grid gridAir;
    private PathRequestManager floorManager;
    private PathRequestManager floorWallRoofManager;
    private PathRequestManager airManager;

    [HideInInspector]
    public Vector3 start;
    [HideInInspector]
    public Vector3 end;
    [HideInInspector]
    public float height;

    public void CreateGrids(Vector3 start, Vector3 end, float height)
    {
        grids[0].GetComponent<Grid>().EventCreateGrid(this.useLevelData, start, end, height);
        grids[1].GetComponent<Grid>().EventCreateGrid(this.useLevelData, start, end, height);
        grids[2].GetComponent<Grid>().EventCreateGrid(this.useLevelData, start, end, height);

        gridFloor = grids[0].GetComponent<Grid>();
        gridFloorWallRoof = grids[1].GetComponent<Grid>();
        gridAir = grids[2].GetComponent<Grid>();

        floorManager = grids[0].GetComponent<PathRequestManager>();
        floorWallRoofManager = grids[1].GetComponent<PathRequestManager>();
        airManager = grids[2].GetComponent<PathRequestManager>();
    }

    public void RequestAirPath(Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback)
    {
        airManager.RequestPath(pathStart, pathEnd, callback);
    }

    public Grid GridAir()
    {
        return gridAir;
    }

    public void UpdateAirGrid(Vector3 center, Vector3 size, int id)
    {
        gridAir.UpdateArea(center, size, id);
    }

    public void RequestFloorPath(Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback)
    {
        floorManager.RequestPath(pathStart, pathEnd, callback);
    }

    public Grid GridFloor()
    {
        return gridFloor;
    }

    public void UpdateFloorGrid(Vector3 center, Vector3 size, int id)
    {
        gridFloor.UpdateArea(center, size, id);
    }

    public void RequestFloorWallRoofPath(Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback)
    {
        floorWallRoofManager.RequestPath(pathStart, pathEnd, callback);
    }

    public Grid GridFloorWallRoof()
    {
        return gridFloorWallRoof;
    }

    public void UpdateFloorWallRoof(Vector3 center, Vector3 size, int id)
    {
        gridFloorWallRoof.UpdateArea(center, size, id);
    }
}
