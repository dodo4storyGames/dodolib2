﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PathRequestManager : MonoBehaviour
{
    //public static PathRequestManager instance;
    public bool simplifyPath;

    private Pathfinding pathfinding;
    private PathRequest currentPathRequest;
    private Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
    private bool isProcessingPath;

    private void Awake()
    {
        //instance = this;
        pathfinding = GetComponent<Pathfinding>();
    }

    public void CreateGrid(Vector3 start, Vector3 end, float worldHeight)
    {
        pathfinding.Grid.CreateGrid(start, end, worldHeight);
    }

    public void RequestPath(Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback)
    {
        PathRequest newRequest = new PathRequest(pathStart, pathEnd, simplifyPath, callback);
        pathRequestQueue.Enqueue(newRequest);
        TryProcessNext();
    }

    private void TryProcessNext()
    {
        if (isProcessingPath == false && pathRequestQueue.Count > 0)
        {
            currentPathRequest = pathRequestQueue.Dequeue();
            isProcessingPath = true;
            pathfinding.StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd, currentPathRequest.simplifyPath);
        }
    }

    public void FinishProcessingPath(Vector3[] path, bool sucess)
    {
        currentPathRequest.callback(path, sucess);
        isProcessingPath = false;
        TryProcessNext();
    }

    private struct PathRequest
    {
        public Vector3 pathStart;
        public Vector3 pathEnd;
        public bool simplifyPath;
        public Action<Vector3[], bool> callback;

        public PathRequest(Vector3 pathStart, Vector3 pathEnd, bool simplifyPath, Action<Vector3[], bool> callback)
        {
            this.pathStart = pathStart;
            this.pathEnd = pathEnd;
            this.simplifyPath = simplifyPath;
            this.callback = callback;
        }
    }
}
