﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Node : IHeapItem<Node>
{
    public bool walkable;
    public bool walkableBuffor;
    public int maxNeighbours;
    public int minNeighbours;
    public Vector3 worldPosition;
    public int gridX;
    public int gridY;
    public int gridZ;
    public Node parent;
    public int gCost; // distance from starting node
    public int hCost; // (heuristic) distance from end node
    public int movementPenalty;
    public bool isFloor;
    public bool isRoof;
    public bool toDelete;
    public int occupied;

    private HashSet<Node> neighbours = new HashSet<Node>();
    private int heapIndex;

    public Node(bool walkable, Vector3 worldPosition, int gridX, int gridY, int gridZ, int movementPenalty, int maxNeighbours, int minNeighbours)
    {
        this.walkable = walkable;
        this.worldPosition = worldPosition;
        this.gridX = gridX;
        this.gridY = gridY;
        this.gridZ = gridZ;
        this.movementPenalty = movementPenalty;
        this.maxNeighbours = maxNeighbours;
        this.minNeighbours = minNeighbours;
    }

    public Node(bool walkable, Vector3 worldPosition, int gridX, int gridY, int gridZ, int movementPenalty, bool isFloor, bool isRoof)
    {
        this.walkable = walkable;
        this.worldPosition = worldPosition;
        this.gridX = gridX;
        this.gridY = gridY;
        this.gridZ = gridZ;
        this.movementPenalty = movementPenalty;
        this.isFloor = isFloor;
        this.isRoof = isRoof;
    }

    public HashSet<Node> Neighbours { get { return neighbours; } }

    /// <summary>
    /// F Cost = G cost + H cost
    /// </summary>
    public int FCost { get { return gCost + hCost; } }

    public int HeapIndex
    {
        get { return heapIndex; }
        set { heapIndex = value; }
    }

    // RayCast in all 26 directions to determine valid 
    public void InitializeConnections(Grid gridNode, bool air)
    {
        RaycastHit hit;
        Ray ray;
        Node[,,] grid = gridNode.GetGrid;

        if (walkable == false)
        {
            return;
        }

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (x == 0 && y == 0 && z == 0)
                    {
                        continue;
                    }
                    int checkX = gridX + x;
                    int checkY = gridY + y;
                    int checkZ = gridZ + z;
                    if (checkX >= 0 && checkX < gridNode.GridSizeX && checkY >= 0 && checkY < gridNode.GridSizeY && checkZ >= 0 && checkZ < gridNode.GridSizeZ)
                    {
                        Vector3 direction = new Vector3(x, z, y);
                        ray = new Ray(worldPosition, direction);
                        bool add = true;
                        float maxDistance = 0;
                        // ustalanie długosci raya 
                        if (direction == Vector3.forward || direction == Vector3.left || direction == Vector3.back || direction == Vector3.right || direction == Vector3.up || direction == Vector3.down)
                        {
                            maxDistance = gridNode.NormalDistance;
                        }
                        else if (direction == new Vector3(1, 1, -1) || direction == new Vector3(1, 1, 1) || direction == new Vector3(-1, 1, -1) || direction == new Vector3(-1, 1, 1) ||
                                 direction == new Vector3(-1, -1, -1) || direction == new Vector3(-1, -1, 1) || direction == new Vector3(1, -1, -1) || direction == new Vector3(1, -1, 1))
                        {
                            maxDistance = gridNode.Diagonal3DDistance;
                        }
                        else
                        {
                            maxDistance = gridNode.DiagonalDistance;
                        }
                        // jeżeli w coś trafi
                        if (Physics.Raycast(ray, out hit, maxDistance, gridNode.obstacleLayers))
                        {
                            add = false;
                        }
                        // jeżeli prowadzi do obiektu po którym nie da się poruszać
                        if (grid[checkX, checkY, checkZ] != null && grid[checkX, checkY, checkZ].walkable == false)
                        {
                            add = false;
                        }
                        // jeżeli w nic nie trafiłem lub trafiony obiekt jest dozwolony to dodaje sąsiada
                        if (add == true && grid[checkX, checkY, checkZ] != null)
                        {
                            neighbours.Add(grid[checkX, checkY, checkZ]);
                        }
                    }
                }
            }
        }
        //if (neighbours.Count >= maxNeighbours || neighbours.Count <= minNeighbours)
        //{
        //    walkableBuffor = false;
        //}
        //else
        //{
        //    walkableBuffor = true;
        //}
        //if(air && neighbours.Count >= maxNeighbours)
        //{
        //    walkableBuffor = true;
        //}
    }

    /// <summary>
    /// Bierze pod uwage tylko sasiadow, nie sprawdza kolizji
    /// </summary>
    /// <param name="gridNode"></param>
    /// <param name="air"></param>
    public void InitializeConnectionsFAST(Grid gridNode, bool air)
    {
        Node[,,] grid = gridNode.GetGrid;

        if (walkable == false)
        {
            return;
        }

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (x == 0 && y == 0 && z == 0)
                    {
                        continue;
                    }
                    int checkX = gridX + x;
                    int checkY = gridY + y;
                    int checkZ = gridZ + z;
                    if (checkX >= 0 && checkX < gridNode.GridSizeX && checkY >= 0 && checkY < gridNode.GridSizeY && checkZ >= 0 && checkZ < gridNode.GridSizeZ)
                    {
                        // obiekt jest dozwolony to dodaje sąsiada
                        if (grid[checkX, checkY, checkZ] != null && grid[checkX, checkY, checkZ].walkable == true)
                        {
                            neighbours.Add(grid[checkX, checkY, checkZ]);
                        }
                    }
                }
            }
        }
        if (neighbours.Count >= maxNeighbours || neighbours.Count <= minNeighbours)
        {
            walkableBuffor = false;
        }
        else
        {
            walkableBuffor = true;
        }
        if (air && neighbours.Count >= maxNeighbours)
        {
            walkableBuffor = true;
        }
    }

    //private void RemoveNode(Node node)
    //{
    //    neighbours.Remove(node);
    //}

    //public void RemoveNodeFromNeighbours(Node node)
    //{
    //    foreach(Node n in neighbours)
    //    {
    //        n.RemoveNode(node);
    //    }
    //}

    public void AddToNeighbours(Grid gridNode)
    {
        Node[,,] grid = gridNode.GetGrid;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (x == 0 && y == 0 && z == 0)
                    {
                        continue;
                    }
                    int checkX = gridX + x;
                    int checkY = gridY + y;
                    int checkZ = gridZ + z;
                    if (checkX >= 0 && checkX < gridNode.GridSizeX && checkY >= 0 && checkY < gridNode.GridSizeY && checkZ >= 0 && checkZ < gridNode.GridSizeZ)
                    {
                        if (grid[checkX, checkY, checkZ] != null && grid[checkX, checkY, checkZ].walkable == true)
                        {
                            grid[checkX, checkY, checkZ].Neighbours.Add(grid[gridX, gridY, gridZ]);
                        }
                    }
                }
            }
        }
    }

    public void RemoveFromNeighbours(Grid gridNode)
    {
        Node[,,] grid = gridNode.GetGrid;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (x == 0 && y == 0 && z == 0)
                    {
                        continue;
                    }
                    int checkX = gridX + x;
                    int checkY = gridY + y;
                    int checkZ = gridZ + z;
                    if (checkX >= 0 && checkX < gridNode.GridSizeX && checkY >= 0 && checkY < gridNode.GridSizeY && checkZ >= 0 && checkZ < gridNode.GridSizeZ)
                    {
                        if (grid[checkX, checkY, checkZ] != null && grid[checkX, checkY, checkZ].walkable == true)
                        {
                            grid[checkX, checkY, checkZ].Neighbours.Remove(grid[gridX, gridY, gridZ]);
                        }
                    }
                }
            }
        }
    }

    public void SwitchWalkablewithBuffor()
    {
        walkable = walkableBuffor;
    }

    //public void RemoveNotWalkable(Grid gridNode)
    //{
    //    Node[,,] grid = gridNode.GetGrid;
    //    neighbours.Clear();

    //    if (walkable == false || walkableBuffor == false)
    //    {
    //        return;
    //    }

    //    for (int x = -1; x <= 1; x++)
    //    {
    //        for (int y = -1; y <= 1; y++)
    //        {
    //            for (int z = -1; z <= 1; z++)
    //            {
    //                if (x == 0 && y == 0 && z == 0)
    //                {
    //                    continue;
    //                }
    //                int checkX = gridX + x;
    //                int checkY = gridY + y;
    //                int checkZ = gridZ + z;
    //                if (checkX >= 0 && checkX < gridNode.GridSizeX && checkY >= 0 && checkY < gridNode.GridSizeY && checkZ >= 0 && checkZ < gridNode.GridSizeZ)
    //                {
    //                    if (grid[checkX, checkY, checkZ] != null && grid[checkX, checkY, checkZ].walkableBuffor == true)
    //                    {
    //                        neighbours.Add(grid[checkX, checkY, checkZ]);
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}

    public Node GetClosestWalkableNode(Grid gridNode)
    {
        Node[,,] grid = gridNode.GetGrid;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (x == 0 && y == 0 && z == 0)
                    {
                        continue;
                    }
                    int checkX = gridX + x;
                    int checkY = gridY + y;
                    int checkZ = gridZ + z;
                    if (checkX >= 0 && checkX < gridNode.GridSizeX && checkY >= 0 && checkY < gridNode.GridSizeY && checkZ >= 0 && checkZ < gridNode.GridSizeZ)
                    {
                        if (grid[checkX, checkY, checkZ] != null && grid[checkX, checkY, checkZ].walkable == true)
                        {
                            return grid[checkX, checkY, checkZ];
                        }
                    }
                }
            }
        }
        return null;
    }

    public static Node GetClosestWalkableNodeStatic(Grid gridNode, int xx, int yy, int zz)
    {
        Node[,,] grid = gridNode.GetGrid;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (x == 0 && y == 0 && z == 0)
                    {
                        continue;
                    }
                    int checkX = xx + x;
                    int checkY = yy + y;
                    int checkZ = zz + z;
                    if (checkX >= 0 && checkX < gridNode.GridSizeX && checkY >= 0 && checkY < gridNode.GridSizeY && checkZ >= 0 && checkZ < gridNode.GridSizeZ)
                    {
                        if (grid[checkX, checkY, checkZ] != null && grid[checkX, checkY, checkZ].walkable == true)
                        {
                            return grid[checkX, checkY, checkZ];
                        }
                    }
                }
            }
        }
        return null;
    }

    public static Node GetClosestNodeStatic(Grid gridNode, int xx, int yy, int zz)
    {
        Node[,,] grid = gridNode.GetGrid;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    if (x == 0 && y == 0 && z == 0)
                    {
                        continue;
                    }
                    int checkX = xx + x;
                    int checkY = yy + y;
                    int checkZ = zz + z;
                    if (checkX >= 0 && checkX < gridNode.GridSizeX && checkY >= 0 && checkY < gridNode.GridSizeY && checkZ >= 0 && checkZ < gridNode.GridSizeZ)
                    {
                        if (grid[checkX, checkY, checkZ] != null)
                        {
                            return grid[checkX, checkY, checkZ];
                        }
                    }
                }
            }
        }
        return null;
    }

    public int CompareTo(Node other)
    {
        int compare = FCost.CompareTo(other.FCost);
        if (compare == 0)
        {
            compare = hCost.CompareTo(other.hCost);
        }
        return -compare;
    }
}
