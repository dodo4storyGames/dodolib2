﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Pathfinding : MonoBehaviour
{
    private bool simplifyPath;
    public bool useTerrainPenalty;

    private PathRequestManager pathRequestManager;
    private Grid grid;

    public Grid Grid { get { return grid; } }

    private void Awake()
    {
        grid = GetComponent<Grid>();
        pathRequestManager = GetComponent<PathRequestManager>();
    }

    public void StartFindPath(Vector3 pointA, Vector3 pointB, bool simplifyPath)
    {
        this.simplifyPath = simplifyPath;
        StartCoroutine(FindPath(pointA, pointB));
    }

    private IEnumerator FindPath(Vector3 start, Vector3 end)
    {
        Vector3[] wayPoints = new Vector3[1];
        bool pathSuccess = false;

        while (grid.isReady == false)
        {
            yield return null;
        }

        Node startNode = grid.GetNodeFromWorldPoint(start, true);
        Node endNode = grid.GetNodeFromWorldPoint(end, true);

        if ((startNode != null && endNode != null) && endNode.walkable == true && startNode != endNode)
        {
            startNode.parent = startNode;
            Heap<Node> openSet = new Heap<Node>(grid.MaxSize);
            HashSet<Node> closedSet = new HashSet<Node>();

            openSet.Add(startNode);
            while (openSet.Count > 0)
            {
                Node currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if (currentNode == endNode)
                {
                    pathSuccess = true;
                    break;
                }
                foreach (Node neighbour in currentNode.Neighbours)
                {
                    if (!neighbour.walkable || closedSet.Contains(neighbour))
                    {
                        continue;
                    }
                    int newMovmentCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour) + (useTerrainPenalty ? neighbour.movementPenalty : 0);
                    if (newMovmentCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newMovmentCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, endNode);
                        neighbour.parent = currentNode;
                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Add(neighbour);
                        }
                        else
                        {
                            openSet.UpdateItem(neighbour);
                        }
                    }
                }
            }
        }
        yield return null;
        if (pathSuccess == true)
        {
            wayPoints = RetracePath(startNode, endNode);
        }
        else if ((startNode != null && endNode != null) && endNode.walkable == true && startNode == endNode)
        {
            wayPoints[0] = endNode.worldPosition;
            pathSuccess = true;
        }
        pathRequestManager.FinishProcessingPath(wayPoints, pathSuccess);
    }

    private Vector3[] RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        Vector3[] wayPoints;
        if (simplifyPath == true)
        {
            path.Add(startNode);
            wayPoints = SimplifyPath(path);
        }
        else
        {
            wayPoints = ListNodeToArray(path);
        }
        Array.Reverse(wayPoints);

        return wayPoints;
    }

    private Vector3[] ListNodeToArray(List<Node> path)
    {
        List<Vector3> wayPoints = new List<Vector3>();
        for (int i = 0; i < path.Count; i++)
        {
            wayPoints.Add(path[i].worldPosition);
        }
        return wayPoints.ToArray();
    }

    private Vector3[] SimplifyPath(List<Node> path)
    {
        List<Vector3> wayPoints = new List<Vector3>();
        Vector2 directionOld = Vector2.zero;

        for (int i = 1; i < path.Count; i++)
        {
            Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
            if (directionNew != directionOld)
            {
                wayPoints.Add(path[i - 1].worldPosition);
            }
            directionOld = directionNew;
        }
        return wayPoints.ToArray();
    }

    private int GetDistance(Node nodeA, Node nodeB)
    {
        int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);
        int distZ = Mathf.Abs(nodeA.gridZ - nodeB.gridZ);

        if (distX > distY)
        {
            return 14 * distY + 10 * (distX - distY) + distZ * 14;
        }
        else
        {
            return 14 * distX + 10 * (distY - distX) + distZ * 17;
        }
    }
}
