﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Diagnostics;
using UnityEngine.Events;

/// <summary>
/// Aby event GridDone zostal odpalony musza zostac zrobione conajmniej 3 gridy.
/// </summary>
public class Grid : MonoBehaviour
{
    // debug
    public bool debug;
    public string name;
    public float yOffset;
    public float yHeight;
    [Range(0, 1)]
    public float cubeSize = 0.5f;
    public bool drawNavMesh;
    public bool drawGrid;
    public bool drawPenalty;
    private int penaltyMin = int.MaxValue;
    private int penaltyMax = int.MinValue;
    public int zHeight;
    public Vector3 gridDebug;
    // end

    [Space(10)]
    public LayerMask forbiddenLayers;
    public int forbiddenPenalty;
    public LayerMask obstacleLayers;
    public LayerMask floorLayers;
    public LayerMask roofLayers;
    public LayerMask wallLayers;
    public LayerMask airLayers;
    public bool isFloor;
    public bool isRoof;
    public bool isWall;
    public bool isAir;
    public TerrainType[] permittedLayers;
    [Space(10)]
    public Transform gridStart;
    public Transform gridEnd;
    public float radius;
    public float gridSize;
    public float worldHeight;

    private LayerMask walkableMask = new LayerMask();
    private Dictionary<int, int> walkableLayerDictionary = new Dictionary<int, int>();
    private Vector2 gridWorldSize;
    private Vector3 gridWorldCenter;
    public Vector3 GridWorldCenter
    {
        get { return gridWorldCenter; }
    }
    private Node[,,] grid;
    private int gridSizeX;
    private int gridSizeY;
    private int gridSizeZ;
    private float diagonalDistance;
    private float diagonal3DDistance;
    private static int gridCount;

    public bool isReady = false;

    public Node[,,] GetGrid { get { return grid; } }
    public int GridSizeX { get { return gridSizeX; } }
    public int GridSizeY { get { return gridSizeY; } }
    public int GridSizeZ { get { return gridSizeZ; } }
    public float DiagonalDistance { get { return diagonalDistance; } }
    public float NormalDistance { get { return gridSize; } }
    public float Diagonal3DDistance { get { return diagonal3DDistance; } }
    public int MaxSize { get { return gridSizeX * gridSizeY; } }

    public void Awake()
    {
        isReady = false;
        gridCount = 0;
    }

    /// <summary>
    /// Metoda tworzaca grid.
    /// </summary>
    /// <param name="useLevelData">Jeżeli true to start, end, oraz height zostana uswatione wedlug podanych wartosci</param>
    /// <param name="start">Punkt poczatkowy gridu</param>
    /// <param name="end">Punkt koncowy gridu</param>
    /// <param name="height">Wysokosc gridu</param>
    public void EventCreateGrid(bool useLevelData, Vector3 start, Vector3 end, float height)
    {
        if (useLevelData == true)
        {
            gridStart.position = start + new Vector3(0, yOffset, 0);
            gridEnd.position = end + new Vector3(0, yOffset, 0);
            worldHeight = height + yHeight;
        }
        Init();
        StartCoroutine(CreateGrid());
    }

    private void Init()
    {
        gridWorldCenter = (gridStart.position + gridEnd.position) / 2f;
        gridWorldSize.x = Mathf.Abs(gridEnd.position.x - gridStart.position.x);
        gridWorldSize.y = Mathf.Abs(gridEnd.position.z - gridStart.position.z);

        diagonalDistance = Mathf.Sqrt(2) * gridSize;
        diagonal3DDistance = Mathf.Sqrt(3) * gridSize;

        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / gridSize);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / gridSize);
        gridSizeZ = Mathf.RoundToInt(worldHeight / gridSize);

        // ponowna kalkulacja aby grid idealnie pasowal
        float newX = (gridSizeX * gridSize) / 2f;
        float newZ = (gridSizeY * gridSize) / 2f;
        float newHeight = (gridSizeZ * gridSize);
        gridStart.position = new Vector3(gridWorldCenter.x - newX, gridStart.position.y, gridWorldCenter.z - newZ);
        gridEnd.position = new Vector3(gridWorldCenter.x + newX, gridStart.position.y, gridWorldCenter.z + newZ);

        // przypisuje nowe wartosci
        gridWorldSize.x = Mathf.Abs(gridEnd.position.x - gridStart.position.x);
        gridWorldSize.y = Mathf.Abs(gridEnd.position.z - gridStart.position.z);
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / gridSize);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / gridSize);
        // przypisuje wysokosc
        worldHeight = newHeight;

        AddWalkableLayers();
    }

    public void CreateGrid(Vector3 start, Vector3 end, float gridSize, float worldHeight)
    {
        gridStart.position = start;
        gridEnd.position = end;
        this.gridSize = gridSize;
        this.worldHeight = worldHeight;
        Init();
        StartCoroutine(CreateGrid());
    }

    public void CreateGrid(Vector3 start, Vector3 end, float worldHeight)
    {
        gridStart.position = start;
        gridEnd.position = end;
        this.worldHeight = worldHeight;
        Init();
        StartCoroutine(CreateGrid());
    }

    private void AddWalkableLayers()
    {
        walkableLayerDictionary.Clear();
        foreach (TerrainType layer in permittedLayers)
        {
            walkableMask.value |= layer.terrainMask.value;
            walkableLayerDictionary.Add((int)Mathf.Log(layer.terrainMask.value, 2), layer.terrainPenalty);
        }
    }

    private IEnumerator CreateGrid()
    {
        Stopwatch sw = new Stopwatch();
        sw.Start();

        grid = new Node[gridSizeX, gridSizeY, gridSizeZ];
        Vector3 worldBottomLeft = gridWorldCenter - Vector3.right * gridWorldSize.x / 2f - Vector3.forward * gridWorldSize.y / 2f;// - Vector3.up * gridSize / 2f;

        Vector3 worldPoint;
        bool walkable = false;
        int movmentPenalty = 0;
        bool touchFloor = false;
        bool touchRoof = false;
        bool touchWall = false;
        bool touchObstacle = false;
        bool touchAir = false;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                for (int z = 0; z < gridSizeZ; z++)
                {
                    worldPoint = worldBottomLeft + Vector3.right * (x * gridSize + gridSize / 2f) + Vector3.forward * (y * gridSize + gridSize / 2f) + Vector3.up * (z * gridSize + gridSize / 2f);
                    walkable = CheckWalkable(worldPoint);
                    if (walkable == false) continue;

                    if (walkable == true)
                    {
                        touchFloor = (Physics.CheckSphere(worldPoint, radius, floorLayers));
                        touchRoof = (Physics.CheckSphere(worldPoint, radius, roofLayers));
                        touchWall = (Physics.CheckSphere(worldPoint, radius, wallLayers));
                        touchAir = (Physics.CheckSphere(worldPoint, radius, airLayers));
                        touchObstacle = (Physics.CheckSphere(worldPoint, radius, obstacleLayers));
                        movmentPenalty = CheckMovmentPenalty(worldPoint);
                    }
                    else
                    {
                        touchFloor = false;
                        touchRoof = false;
                        movmentPenalty = forbiddenPenalty;
                    }

                    if (touchObstacle || (touchWall && (touchFloor || touchRoof)))
                    {
                        walkable = false;
                    }

                    if(isFloor && isRoof && isWall && (touchWall || touchFloor || touchRoof) && touchAir == false)
                    {
                        continue;
                    }
                    grid[x, y, z] = new Node(walkable, worldPoint, x, y, z, movmentPenalty, touchFloor, touchRoof);
                }
            }
            //yield return new WaitForFixedUpdate();
        }

        //Tworzy połączenia z sąsiadami
        foreach (Node n in grid)
        {
            if (n != null)
            {
                n.InitializeConnectionsFAST(this, isAir);
                NodeToDelete(n);
            }
        }
        foreach (Node n in grid)
        {
            if (n != null && n.toDelete == true)
            {
                grid[n.gridX, n.gridY, n.gridZ].RemoveFromNeighbours(this);
                grid[n.gridX, n.gridY, n.gridZ] = null;
            }
        }

        yield return new WaitForFixedUpdate();
        isReady = true;
        sw.Stop();
        print("Grid " + name + " Created: " + sw.ElapsedMilliseconds + " ms");
        GridCount();
    }

    private static void GridCount()
    {
        gridCount++;
        if(gridCount >= 3)
        {
            EventManager.Instance.TriggerEvent("GridDone");
        }
    }

    private void NodeToDelete(Node n)
    {
        if (n.isFloor == true)
        {
            // tu może wywalić błąd jeżeli z wyjdzie poza zakres tablicy, jest zabezpieczenie wiec nie powinno
            if ((n.gridZ + 1) < gridSizeZ - 1 && grid[n.gridX, n.gridY, n.gridZ + 1] != null)
            {
                n.toDelete = true;
            }
        }
        if (n.isRoof == true)
        {
            // tu może wywalić błąd jeżeli z wyjdzie poza zakres tablicy, jest zabezpieczenie wiec nie powinno
            if (n.gridZ - 1 > 0 && grid[n.gridX, n.gridY, n.gridZ - 1] != null)
            {
                n.toDelete = true;
            }
        }
    }

    public void UpdateNode(Node n, int id)
    {
        if (n != null && (n.occupied == id || n.occupied == 0))
        {
            n.occupied = 0;
            Vector3 worldPoint = n.worldPosition;
            bool walkable = CheckWalkable(worldPoint);
            bool touchFloor = false;
            bool touchRoof = false;
            bool touchWall = false;
            bool touchObstacle = false;

            if (walkable == true)
            {
                touchFloor = (Physics.CheckSphere(worldPoint, radius, floorLayers));
                touchRoof = (Physics.CheckSphere(worldPoint, radius, roofLayers));
                touchWall = (Physics.CheckSphere(worldPoint, radius, wallLayers));
                touchObstacle = (Physics.CheckSphere(worldPoint, radius, obstacleLayers));
            }
            else
            {
                touchFloor = false;
                touchRoof = false;
            }

            if (touchObstacle || (touchWall && (touchFloor || touchRoof)))
            {
                walkable = false;
            }

            n.walkable = walkable;
            n.InitializeConnectionsFAST(this, isAir);
            // update sasiadow
            if (walkable == true)
            {
                //add to Neighbours
                n.AddToNeighbours(this);
            }
            else
            {
                // remove from Neighbours
                n.RemoveFromNeighbours(this);
                n.Neighbours.Clear();
            }
        }
    }

    public void UpdateNodePenalty(Node n, int penalty)
    {
        if (!isReady && grid == null)
        {
            UnityEngine.Debug.LogError("Grid " + name + " not initiated");
            return;
        }

        if (n != null)
        {
            n.movementPenalty = penalty;
            if (n.movementPenalty < 0)
            {
                n.movementPenalty = 0;
            }
        }
    }

    public void UpdateAll()
    {
        if (!isReady && grid == null)
        {
            UnityEngine.Debug.LogError("Grid " + name + " not initiated");
            return;
        }

        Stopwatch sw = new Stopwatch();
        sw.Start();
        foreach (Node node in grid)
        {
            if (node != null)
            {
                UpdateNode(node, 0);
            }
        }
        sw.Stop();
        print("Updated in: " + sw.ElapsedMilliseconds);
    }

    public void UpdateAreaPenalty(Vector3 center, Vector3 size, int penalty)
    {
        if (!isReady && grid == null)
        {
            UnityEngine.Debug.LogError("Grid " + name + " not initiated");
            return;
        }

        int gridX = Mathf.Clamp(Mathf.FloorToInt((center.x - gridWorldCenter.x) / gridSize + gridSizeX / 2f), 0, gridSizeX - 1);
        int gridY = Mathf.Clamp(Mathf.FloorToInt((center.z - gridWorldCenter.z) / gridSize + gridSizeY / 2f), 0, gridSizeY - 1);
        int gridZ = Mathf.Clamp(Mathf.FloorToInt((center.y - gridWorldCenter.y - worldHeight / 2f) / gridSize + gridSizeZ / 2f), 0, gridSizeZ - 1);

        int xi = (int)Mathf.Ceil(size.x) + 1;
        int yi = (int)Mathf.Ceil(size.z) + 1;
        int zi = (int)Mathf.Ceil(size.y) + 1;

        for (int x = -xi; x <= xi; x++)
        {
            for (int y = -yi; y <= yi; y++)
            {
                for (int z = -zi; z <= zi; z++)
                {
                    int checkX = gridX + x;
                    int checkY = gridY + y;
                    int checkZ = gridZ + z;
                    if (checkX >= 0 && checkX < GridSizeX && checkY >= 0 && checkY < GridSizeY && checkZ >= 0 && checkZ < GridSizeZ)
                    {
                        if (grid[checkX, checkY, checkZ] != null)
                        {
                            if (Mathf.Abs(x) == xi || Mathf.Abs(y) == yi || Mathf.Abs(z) == zi)
                            {
                                UpdateNodePenalty(grid[checkX, checkY, checkZ], 0);
                            }
                            else
                            {
                                UpdateNodePenalty(grid[checkX, checkY, checkZ], penalty);
                            }
                        }
                    }
                }
            }
        }
    }

    public void UpdateArea(Vector3 center, Vector3 size, int id)
    {
        if (!isReady && grid == null)
        {
            UnityEngine.Debug.LogError("Grid " + name + " not initiated");
            return;
        }

        int gridX = Mathf.Clamp(Mathf.FloorToInt((center.x - gridWorldCenter.x) / gridSize + gridSizeX / 2f), 0, gridSizeX - 1);
        int gridY = Mathf.Clamp(Mathf.FloorToInt((center.z - gridWorldCenter.z) / gridSize + gridSizeY / 2f), 0, gridSizeY - 1);
        int gridZ = Mathf.Clamp(Mathf.FloorToInt((center.y - gridWorldCenter.y - worldHeight / 2f) / gridSize + gridSizeZ / 2f), 0, gridSizeZ - 1);

        int xi = (int)Mathf.Ceil(size.x);
        int yi = (int)Mathf.Ceil(size.z);
        int zi = (int)Mathf.Ceil(size.y);

        for (int x = -xi; x <= xi; x++)
        {
            for (int y = -yi; y <= yi; y++)
            {
                for (int z = -zi; z <= zi; z++)
                {
                    int checkX = gridX + x;
                    int checkY = gridY + y;
                    int checkZ = gridZ + z;
                    if (checkX >= 0 && checkX < GridSizeX && checkY >= 0 && checkY < GridSizeY && checkZ >= 0 && checkZ < GridSizeZ)
                    {
                        if (grid[checkX, checkY, checkZ] != null)
                        {
                            UpdateNode(grid[checkX, checkY, checkZ], id);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Sprawdza czy w podanym punkcie można prouszać się.
    /// </summary>
    /// <param name="worldPoint"> Punkt (Vector3) w którym sprawdzamy możliwość poruszania.</param>
    /// <returns></returns>
    private bool CheckWalkable(Vector3 worldPoint)
    {
        bool walkable = true;
        walkable = !(Physics.CheckSphere(worldPoint, radius, forbiddenLayers, QueryTriggerInteraction.Collide));
        if (walkable == true)
        {
            walkable = (Physics.CheckSphere(worldPoint, radius, walkableMask, QueryTriggerInteraction.Collide));
        }
        return walkable;
    }

    /// <summary>
    /// Metoda sprawdza czy można poruszać się po danym punkcie.
    /// </summary>
    /// <param name="worldPoint"> Punkt który zostaje sprawdzony</param>
    /// <returns></returns>
    public bool Walkable(Vector3 worldPoint)
    {
        Node n = GetNodeFromWorldPoint(worldPoint, false);
        if (n != null)
        {
            return n.walkable;
        }
        else
        {
            return false;
        }
    }

    private int CheckMovmentPenalty(Vector3 worldPoint)
    {
        int penalty = 0;
        int tmpPenalty = 0;
        Collider[] hitColliders = Physics.OverlapSphere(worldPoint, radius);
        foreach (Collider col in hitColliders)
        {
            tmpPenalty = 0;
            walkableLayerDictionary.TryGetValue(col.gameObject.layer, out tmpPenalty);
            penalty += tmpPenalty;
        }

        if (penalty > penaltyMax)
        {
            penaltyMax = penalty;
        }
        if (penalty < penaltyMin)
        {
            penaltyMin = penalty;
        }
        return penalty;
    }

    public Node GetNodeFromWorldPoint(Vector3 worldPosition, bool nearest)
    {
        if (!isReady && grid == null)
        {
            UnityEngine.Debug.LogError("Grid " + name + " not initiated");
            return null;
        }

        //                                                    - gridWorldCenter. //korekta gdy grid nie zaczyna się w (0,0)
        int x = Mathf.Clamp(Mathf.FloorToInt((worldPosition.x - gridWorldCenter.x) / gridSize + gridSizeX / 2f), 0, gridSizeX - 1);
        int y = Mathf.Clamp(Mathf.FloorToInt((worldPosition.z - gridWorldCenter.z) / gridSize + gridSizeY / 2f), 0, gridSizeY - 1);
        int z = Mathf.Clamp(Mathf.FloorToInt((worldPosition.y - gridWorldCenter.y - worldHeight / 2f) / gridSize + gridSizeZ / 2f), 0, gridSizeZ - 1);

        if (nearest == true)
        {
            if (grid[x, y, z] != null)
            {
                if (grid[x, y, z].walkable == true)
                {
                    return grid[x, y, z];
                }
                else
                {
                    return grid[x, y, z].GetClosestWalkableNode(this);
                }
            }
            else
            {
                // node jest null
                int zzPlus = Mathf.Clamp(z + 1, 0, gridSizeZ - 1);
                int zzMinus = Mathf.Clamp(z - 1, 0, gridSizeZ - 1);
                if (grid[x, y, zzMinus] != null && grid[x, y, zzMinus].walkable)
                {
                    return grid[x, y, zzMinus];
                }
                else if (grid[x, y, zzPlus] != null && grid[x, y, zzPlus].walkable)
                {
                    return grid[x, y, zzPlus];
                }
                else
                {
                    return Node.GetClosestWalkableNodeStatic(this, x, y, z);
                }
            }
        }
        else
        {
            if (grid[x, y, z] != null)
            {
                return grid[x, y, z];
            }
            else
            {
                // node jest null
                int zzPlus = Mathf.Clamp(z + 1, 0, gridSizeZ - 1);
                int zzMinus = Mathf.Clamp(z - 1, 0, gridSizeZ - 1);
                if (grid[x, y, zzMinus] != null)
                {
                    return grid[x, y, zzMinus];
                }
                else if (grid[x, y, zzPlus] != null)
                {
                    return grid[x, y, zzPlus];
                }
                else
                {
                    return Node.GetClosestNodeStatic(this, x, y, z);
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (debug == true)
        {
            if (grid != null && isReady == true)
            {
                if (drawGrid || drawPenalty)
                {
                    foreach (Node n in grid)
                    {
                        if (n != null)
                        {
                            if (n.movementPenalty > penaltyMax)
                            {
                                penaltyMax = n.movementPenalty;
                            }
                            if (n.movementPenalty < penaltyMin)
                            {
                                penaltyMin = n.movementPenalty;
                            }
                            if (n.gridZ == zHeight)
                            {
                                if (drawPenalty)
                                {
                                    Gizmos.color = Color.Lerp(Color.white, Color.black, Mathf.InverseLerp(penaltyMin, penaltyMax, n.movementPenalty));
                                    Gizmos.color = (n.walkable) ? Gizmos.color : Color.red;
                                    Gizmos.DrawCube(n.worldPosition, Vector3.one * (gridSize - gridSize * (1 - cubeSize)));
                                }
                                else
                                {
                                    Gizmos.color = (n.walkable) ? Color.white : Color.red;
                                    Gizmos.DrawCube(n.worldPosition, Vector3.one * (gridSize - gridSize * (1 - cubeSize)));
                                }
                            }
                            else if (zHeight == -2)
                            {
                                if (drawPenalty)
                                {
                                    Gizmos.color = Color.Lerp(Color.white, Color.black, Mathf.InverseLerp(penaltyMin, penaltyMax, n.movementPenalty));
                                    Gizmos.color = (n.walkable) ? Gizmos.color : Color.red;
                                    Gizmos.DrawCube(n.worldPosition, Vector3.one * (gridSize - gridSize * (1 - cubeSize)));
                                }
                                else
                                {
                                    Gizmos.color = (n.walkable) ? Color.white : Color.red;
                                    Gizmos.DrawCube(n.worldPosition, Vector3.one * (gridSize - gridSize * (1 - cubeSize)));
                                }
                            }

                            if (n.gridX == gridDebug.x && n.gridY == gridDebug.y && n.gridZ == gridDebug.z)
                            {
                                Gizmos.color = (n.walkable) ? Color.green : Color.red;
                                Gizmos.DrawCube(n.worldPosition, Vector3.one * (gridSize - gridSize * (1 - cubeSize)) * 1.5f);
                                foreach (Node nn in n.Neighbours)
                                {
                                    Gizmos.color = Color.magenta;
                                    Gizmos.DrawLine(n.worldPosition, nn.worldPosition);
                                }
                            }
                        }
                    }
                }

                if (drawNavMesh)
                {
                    foreach (Node n in grid)
                    {
                        if (n != null)
                        {
                            if (n.gridZ == zHeight)
                            {
                                foreach (Node nn in n.Neighbours)
                                {
                                    Gizmos.color = Color.magenta;
                                    Gizmos.DrawLine(n.worldPosition, nn.worldPosition);
                                }
                            }
                            else if (zHeight == -2)
                            {
                                foreach (Node nn in n.Neighbours)
                                {
                                    Gizmos.color = Color.magenta;
                                    Gizmos.DrawLine(n.worldPosition, nn.worldPosition);
                                }
                            }
                        }
                    }
                }
                Gizmos.color = Color.white;
                Gizmos.DrawWireCube(gridWorldCenter + Vector3.up * worldHeight / 2f, new Vector3(gridWorldSize.x, worldHeight, gridWorldSize.y));
            }
        }
    }
}
