﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class PlayerTestPath : MonoBehaviour
{
    private const float pathUpdateMoveThreshold = 0.5f;
    // debug
    public bool drawPath;
    private Stopwatch sw;

    [Space(10)]
    public bool simplifyPath;
    public bool useCurve;
    public float turnDistance;
    public float turnSpeed;
    public float turnStartSpeed;
    public float moveSpeed;

	private PathfinderPath pathCurve;
    private Vector3[] path;
    private int targetIndex;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                sw = new Stopwatch();
                sw.Start();
                //PathRequestManager.RequestPath(transform.position, hit.point, OnPathFound);
            }
        }
    }

    private void OnPathFound(Vector3[] wayPoints, bool pathFound)
    {
        sw.Stop();
        print("Path found " + pathFound + ": " + sw.ElapsedMilliseconds + " ms");
        if (pathFound == true)
        {
            StopAllCoroutines();
			pathCurve = new PathfinderPath(wayPoints, transform.position, turnDistance);
            path = wayPoints;
            if (useCurve)
            {
                StartCoroutine("FollowPathCurve");
            }
            else
            {
                targetIndex = 0;
                StartCoroutine("FollowPath");
            }
        }
    }

    private IEnumerator FollowPathCurve()
    {
        bool followingPath = true;
        bool rotateFirst = true;
        int pathIndex = 0;

        while (true)
        {
            Vector2 pos2D = new Vector2(transform.position.x, transform.position.z);
            while (pathCurve.turnBoundraies[pathIndex].HasCrossedLine(pos2D))
            {
                if (pathIndex == pathCurve.finishLineIndex)
                {
                    followingPath = false;
                    break;
                }
                else
                {
                    pathIndex++;
                }
            }
            if (followingPath)
            {
                if (rotateFirst)
                {
                    Quaternion targetRotation = Quaternion.LookRotation(pathCurve.lookPoints[pathIndex] - transform.position);
                    transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * turnStartSpeed);
                    if (Mathf.Abs(transform.rotation.eulerAngles.y - targetRotation.eulerAngles.y) < 20f)
                    {
                        rotateFirst = false;
                    }
                } else
                {
                    Quaternion targetRotation = Quaternion.LookRotation(pathCurve.lookPoints[pathIndex] - transform.position);
                    transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * turnSpeed);
                    transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed, Space.Self);
                }
            }
            yield return null;
        }
    }

    private IEnumerator FollowPath()
    {
        Vector3 currentWayPoint = path[0];
        while (true)
        {
            if (transform.position == currentWayPoint)
            {
                targetIndex++;
                if (targetIndex >= path.Length)
                {
                    yield break;
                }
                currentWayPoint = path[targetIndex];
            }
            transform.position = Vector3.MoveTowards(transform.position, currentWayPoint, moveSpeed * Time.deltaTime);
            yield return null;
        }
    }

    private void OnDrawGizmos()
    {
        if (path != null)
        {
            if (drawPath)
            {
                if (useCurve && pathCurve != null)
                {
                    pathCurve.DrawWithGizmos(Vector3.one * transform.localScale.x * 0.5f, transform.localScale.x * 2f);
                }
                else
                {
                    for (int i = 0; i < path.Length; i++)
                    {
                        Gizmos.color = Color.green;
                        Gizmos.DrawCube(path[i], Vector3.one * transform.localScale.x * 0.5f);
                        if (i + i < path.Length)
                        {
                            Gizmos.DrawLine(path[i], path[i + 1]);
                        }
                    }
                }
            }
        }
    }
}
