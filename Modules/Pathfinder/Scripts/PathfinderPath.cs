﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfinderPath
{
    public readonly Vector3[] lookPoints;
    public readonly Line[] turnBoundraies;
    public readonly int finishLineIndex;

	public PathfinderPath(Vector3[] wayPoints, Vector3 start, float turnDistance)
    {
        lookPoints = wayPoints;
        turnBoundraies = new Line[lookPoints.Length];
        finishLineIndex = turnBoundraies.Length - 1;

        Vector2 previousPoint = V3ToV2(start);
        for (int i = 0; i < lookPoints.Length; i++)
        {
            Vector2 currentPoint = V3ToV2(lookPoints[i]);
            Vector2 dirToCurrentPoint = (currentPoint - previousPoint).normalized;
            Vector2 turnBoundrypoint = (i == finishLineIndex) ? currentPoint : currentPoint - dirToCurrentPoint * turnDistance;
            turnBoundraies[i] = new Line(turnBoundrypoint, previousPoint - dirToCurrentPoint * turnDistance);
            previousPoint = turnBoundrypoint;
        }
    }

    private Vector2 V3ToV2(Vector3 v3)
    {
        return new Vector2(v3.x, v3.z);
    }

    public void DrawWithGizmos(Vector3 cubeSize, float perpendicularLineLength)
    {
        Gizmos.color = Color.black;
        foreach(Vector3 p in lookPoints)
        {
            Gizmos.DrawCube(p + Vector3.up, cubeSize);
        }

        for (int i = 0; i < lookPoints.Length; i++)
        {
            if (i + 1 < lookPoints.Length)
            {
                Gizmos.DrawLine(lookPoints[i] + Vector3.up, lookPoints[i + 1] + Vector3.up);
            }
        }

        Gizmos.color = Color.magenta;
        foreach(Line l in turnBoundraies)
        {
            l.DrawWithGizmos(perpendicularLineLength);
        }
    }
}
