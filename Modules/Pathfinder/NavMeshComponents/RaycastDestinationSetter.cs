﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastDestinationSetter : MonoBehaviour
{
    private DirectedAgent agent;

	public Transform target;


    private void Awake()
    {
        agent = GetComponent<DirectedAgent>();
		//StartCoroutine (StartAgent());
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out hit))
            {
				agent.MoveToLocation(target.position);
			}
        }  



    }

	IEnumerator StartAgent()
	{
		yield return new WaitForSeconds (3f);
		agent.MoveToLocation (target.position);
	}
}
