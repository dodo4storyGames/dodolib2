﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Diagnostics;

public class NavigationBaker : MonoBehaviour
{
    private NavMeshSurface surface;
    private Stopwatch sw;

    private void Awake()
    {
        sw = new Stopwatch();
        surface = GetComponent<NavMeshSurface>();
        BuildNavMesh();
    }

    private void BuildNavMesh()
    {
        sw.Start();
        surface.BuildNavMesh();
        sw.Stop();
        print("NavMesh Created: " + sw.ElapsedMilliseconds + " ms");
        sw.Reset();
    }
}
