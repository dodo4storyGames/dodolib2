﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DirectedAgent : MonoBehaviour
{
    private NavMeshAgent agent;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
    }

    public void MoveToLocation(Vector3 targetPoint)
    {
        if (agent.isOnNavMesh)
        {
            agent.destination = targetPoint;
            agent.isStopped = false;
        }
        else
        {
            print("Brak Mesha!");
        }
    }

    void OnDrawGizmos()
    {
        if (agent != null && agent.path != null)
        {
            var path = agent.path;

            for (int i = 0; i < path.corners.Length - 1; i++)
            {
                Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);
            }
        }
    }
}
