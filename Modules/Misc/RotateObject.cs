﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {

    [SerializeField]
    Vector3 rotation = new Vector3(0f, 0f, 0f);

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.localEulerAngles = transform.localEulerAngles + (rotation * Time.deltaTime);

	}
}
