﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTransform : MonoBehaviour
{
    [SerializeField]
    Transform target;

    [SerializeField]
    bool inverse = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (target != null)
        {
            if (!inverse)
                transform.LookAt(target);
            else
                transform.rotation = Quaternion.LookRotation(transform.position - target.position);
        }
    }
}
