[System.Serializable]
public class LocalizationData
{
    public LocalizationItem[] items;
}

[System.Serializable]
public class LocalizationItem
{
    public string key;
    public string value;
}

[System.Serializable]
public class LanguageIdentifier
{
    public string ID; // e.g.: "PL", "ENG"
    public string pathToTranslations; // Relative to Resources folder.
}
