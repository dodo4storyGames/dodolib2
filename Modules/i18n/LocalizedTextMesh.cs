using UnityEngine;

public class LocalizedTextMesh : LocalizedComponent {
    private TextMesh textMesh;

    protected override void Awake()
    {
        base.Awake();
        textMesh = GetComponent<TextMesh>();
    }

    // TODO: Allow switching font for specific languages. Use LocalizationManager.Instance.currentLanguage.
    protected override void UpdateSelf()
    {
        string localizedText = LocalizationManager.Instance.GetTranslation(key);

        if (localizedText != null)
        {
            textMesh.text = localizedText;
        }
    }

    // TODO: RTL support?
}
