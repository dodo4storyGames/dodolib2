using UnityEngine;

public abstract class LocalizedComponent : MonoBehaviour {
    [SerializeField]
    protected string key;

    protected virtual void Awake()
    {
        EventManager.Instance.StartListening("i18n:LanguageChanged", UpdateSelf);
    }

    private void Start()
    {
        UpdateSelf();
    }

    private void OnEnable()
    {
        UpdateSelf();
    }

    protected virtual void UpdateSelf() { }
}
