using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : LocalizedComponent {
    private Text text;

    protected override void Awake()
    {
        base.Awake();
        text = GetComponent<Text>();
    }

    // TODO: Allow switching font for specific languages. Use LocalizationManager.Instance.currentLanguage.
    protected override void UpdateSelf()
    {
        string localizedText = LocalizationManager.Instance.GetTranslation(key);

        if (localizedText != null)
        {
            text.text = localizedText;
        }
    }

    // TODO: listen to some event about switching RTL
    private void SwitchAlignment(bool toRTL)
    {
        switch (text.alignment)
        {
            case TextAnchor.UpperLeft:
                if (toRTL)
                {
                    text.alignment = TextAnchor.UpperRight;
                }

                break;
            case TextAnchor.MiddleLeft:
                if (toRTL)
                {
                    text.alignment = TextAnchor.MiddleRight;
                }

                break;
            case TextAnchor.LowerLeft:
                if (toRTL)
                {
                    text.alignment = TextAnchor.LowerRight;
                }

                break;
            case TextAnchor.UpperRight:
                if (!toRTL)
                {
                    text.alignment = TextAnchor.UpperLeft;
                }

                break;
            case TextAnchor.MiddleRight:
                if (!toRTL)
                {
                    text.alignment = TextAnchor.MiddleLeft;
                }

                break;
            case TextAnchor.LowerRight:
                if (!toRTL)
                {
                    text.alignment = TextAnchor.LowerLeft;
                }

                break;
        }
    }
}
