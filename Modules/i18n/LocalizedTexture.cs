using UnityEngine;

public class LocalizedTexture : LocalizedComponent {
    private Renderer renderer;

    protected override void Awake()
    {
        base.Awake();
        renderer = GetComponent<Renderer>();
    }

    protected override void UpdateSelf()
    {
        string texturePath = LocalizationManager.Instance.GetTranslation(key);

        if (texturePath != null)
        {
            Texture texture = (Texture) Resources.Load(texturePath);

            if (texture != null)
            {
                renderer.material.SetTexture("_MainTex", texture); // TODO: may need something else than "_MainTex" sometimes I guess
            }
        }
    }
}
