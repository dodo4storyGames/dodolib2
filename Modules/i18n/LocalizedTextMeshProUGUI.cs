using TMPro;
using UnityEngine;

public class LocalizedTextMeshProUGUI : LocalizedComponent {
    private TextMeshProUGUI textMesh;
    private TMP_FontAsset initialFontAsset;

    protected override void Awake()
    {
        base.Awake();
        textMesh = GetComponent<TextMeshProUGUI>();
        initialFontAsset = textMesh.font;
    }

    // TODO: Allow switching font for specific languages. Use LocalizationManager.Instance.currentLanguage.
    protected override void UpdateSelf()
    {
        // TODO: handle it better than this
        if (LocalizationManager.Instance.currentLanguage == "RU")
        {
            textMesh.font = LocalizationManager.Instance.russianTMP_FontAsset;
        } else
        {
            textMesh.font = initialFontAsset;
        }

        string localizedText = LocalizationManager.Instance.GetTranslation(key);

        if (localizedText != null)
        {
            textMesh.text = localizedText;
        }
    }

    // TODO: RTL support?
}
