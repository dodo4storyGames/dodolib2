using UnityEngine;
using UnityEngine.UI;

public class LocalizedImage : LocalizedComponent {
    private Image image;

    protected override void Awake()
    {
        base.Awake();
        image = GetComponent<Image>();
    }

    protected override void UpdateSelf()
    {
        string imagePath = LocalizationManager.Instance.GetTranslation(key);

        if (imagePath != null)
        {
            Sprite sprite = Resources.Load<Sprite>(imagePath);

            if (image != null)
            {
                image.sprite = sprite;
            }
        }
    }
}
