using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Localization Manager responsible for providing translations as a key/value pairs kept in a dictionary.
/// Every LocalizedComponent has its key as a string and can get a translation using the key.
/// Translations files and other resources as textures etc should be stored in Resources folder.
/// </summary>
public class LocalizationManager : Singleton<LocalizationManager> {
    private Dictionary<string, string> translations;
    [SerializeField]
    private string translationsPath;
    [SerializeField]
    private LanguageIdentifier[] Languages;

    public TMP_FontAsset defaultFontAsset;
    public TMP_FontAsset russianTMP_FontAsset;
    
    public string currentLanguage { get; set; }

    // TODO
    public Text debugText;

    private void Start()
    {
        if (Languages.Length > 0)
        {
            currentLanguage = Languages[0].ID;
            LoadTranslations(Path.Combine(translationsPath, Languages[0].pathToTranslations));
            DontDestroyOnLoad(gameObject);
        } else
        {
            Debug.LogError("No languages defined!");
        }
    }

    /// <summary>
    /// Loads translations for a specified language from a JSON file.
    /// Path to the JSON file should be defined in inspector on this class.
    /// </summary>
    /// <param name="lang">Language ID, e.g.: "PL", "ENG"</param>
    public void SwitchLanguage(string lang)
    {
        LanguageIdentifier newLanguage = (LanguageIdentifier) Languages.Where(item => item.ID == lang).FirstOrDefault();
        
        if (newLanguage != null)
        {
            currentLanguage = newLanguage.ID;
            LoadTranslations(Path.Combine(translationsPath, newLanguage.pathToTranslations));
        }
    }

    /// <summary>
    /// Gets a translated text or path for the given key.
    /// </summary>
    /// <param name="key">Translation identifier.</param>
    /// <returns>Value corresponding to the given key from current translations dictionary.</returns>
    /// <todo>Support params in text?</todo>
    public string GetTranslation(string key)
    {
        if (translations != null && translations.ContainsKey(key))
        {
            return translations[key];
        }

        return null;
    }

    private void LoadTranslations(string filePath)
    {
        translations = new Dictionary<string, string>();
        string fileContents = GetFileContents(filePath);

        if (fileContents != null)
        {
            LocalizationData loadedTranslations = JsonUtility.FromJson<LocalizationData>(fileContents);

            for (int i = 0; i < loadedTranslations.items.Length; i++)
            {
                translations.Add(loadedTranslations.items[i].key, loadedTranslations.items[i].value);
            }

            Debug.Log("Loaded translations, dictionary contains " + translations.Count + " entries.");
            EventManager.Instance.TriggerEvent("i18n:LanguageChanged");
        } else
        {
            Debug.LogError("Cannot load translations.");
        }
    }

    private string GetFileContents(string filePath)
    {
        filePath = filePath.Replace(".json", "");
        TextAsset translationsFile = Resources.Load<TextAsset>(filePath);

        if (translationsFile != null)
        {
            return translationsFile.ToString();
        } else
        {
            return null;
        }
    }
}
