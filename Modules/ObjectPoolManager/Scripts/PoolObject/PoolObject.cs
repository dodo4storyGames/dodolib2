using UnityEngine;

public class PoolObject : MonoBehaviour {

	bool retunrDisabledObject = false;

    Transform parent;

    public void SetParent(Transform parent)
    {
        this.parent = parent;
    }

    public void ReturnToPool()
    {
		transform.parent = parent;
		gameObject.SetActive (false);
        
    }

	void OnDisable()
	{
        ReturnToPool();
		//Invoke ("ReturnToPool", 0.1f);
	}
}
