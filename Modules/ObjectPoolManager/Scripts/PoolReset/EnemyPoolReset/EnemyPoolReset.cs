using UnityEngine;

public class EnemyPoolReset : PoolReset
{
    public void DestroyMe()
    {
        Deactive();
    }

    public override void Reset()
    {

    }
}
