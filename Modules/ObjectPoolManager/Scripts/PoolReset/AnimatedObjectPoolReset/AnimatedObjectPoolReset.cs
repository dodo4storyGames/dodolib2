using UnityEngine;

public class AnimatedObjectPoolReset : PoolReset
{
    Animator animator;

    public override void Reset()
    {
        animator = GetComponent<Animator>();
        animator.Play(0, -1, 0f);
    }

}
