﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToPoolAfterTime : MonoBehaviour {

	[SerializeField]
	float lifeTime = 1f;
	float timer = 0f;

	void OnEnable()
	{
		timer = 0f;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (timer > lifeTime) {
			PoolObject poolObject = GetComponent<PoolObject>();
			poolObject.ReturnToPool ();
		}
	}
}
