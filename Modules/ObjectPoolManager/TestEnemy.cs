using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEnemy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        GetComponent<WaveObject>().AddPoints();
        GetComponent<EnemyPoolReset>().DestroyMe();
        ObjectPoolManager.Instance.SpawnPoolObject("Explosion1", transform.position);
    }
}
