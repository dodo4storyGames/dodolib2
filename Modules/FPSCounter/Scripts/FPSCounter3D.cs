using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(TextMesh))]
public class FPSCounter3D : MonoBehaviour {

    [SerializeField]
    float updateInterval = 0.5f;

    float accum = 0f;
    float timeLeft = 0f;
    int frames = 0;

    TextMesh fpsCounter;

    [SerializeField]
    float lowFPS = 60;
    [SerializeField]
    float poorFPS = 30;

	void Start ()
    {
        fpsCounter = GetComponent<TextMesh>();
        timeLeft = updateInterval;
	}
	
	void Update ()
    {
        timeLeft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        ++frames;

        if (timeLeft <= 0f)
        {
            float fps = accum / frames;
            UpdateCumulativeMovingAverageFPS(fps);
            string format = fps.ToString("F0") + " FPS";

            if (fps < poorFPS)
            {
                //fpsCounter.color = Color.red;
                format = "<color=red>"+format+"</color>";
            }
            else if (fps < lowFPS)
            {
                //fpsCounter.color = Color.yellow;
                format = "<color=yellow>"+format+"</color>";
            }
            else
            {
                //fpsCounter.color = Color.green;
                format = "<color=green>"+format+"</color>";
            }

            fpsCounter.text = format;

            timeLeft = updateInterval;
            accum = 0f;
            frames = 0;
        }
	}

    int qty = 0;
    float currentAvgFPS = 0f;

    void UpdateCumulativeMovingAverageFPS(float lastFPS)
    {
        ++qty;
        currentAvgFPS += (lastFPS - currentAvgFPS)/(float)qty;
    }

    void OnApplicationQuit()
    {
        string endMsg = "Game ends with average fps: " + currentAvgFPS;

        if (currentAvgFPS < poorFPS)
        {
            Debug.LogError(endMsg);
        }
        else if (currentAvgFPS < lowFPS)
        {
            Debug.LogWarning(endMsg);
        }
        else
        {
            Debug.Log(endMsg);
        }

    }
}
