using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FPSCounter : MonoBehaviour {

    [SerializeField]
    float updateInterval = 0.5f;

    float accum = 0f;
    float timeLeft = 0f;
    int frames = 0;

    Text fpsCounter;

	void Start ()
    {
        fpsCounter = GetComponent<Text>();
        timeLeft = updateInterval;
	}
	
	void Update ()
    {
        timeLeft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        ++frames;

        if (timeLeft <= 0f)
        {
            float fps = accum / frames;
            string format = fps.ToString("F2") + " FPS";
            fpsCounter.text = format;

            if (fps < 10)
            {
                fpsCounter.material.color = Color.red;
            }
            else if (fps < 30)
            {
                fpsCounter.material.color = Color.yellow;
            }
            else
            {
                fpsCounter.material.color = Color.green;
            }

            timeLeft = updateInterval;
            accum = 0f;
            frames = 0;
        }
	}
}
