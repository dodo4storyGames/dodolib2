using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomizer<T> {

    List<T> objectList = new List<T>();
    List<float> propabilityList = new List<float>();

    public Randomizer()
    {

    }

    public void AddObject(T obj, float propability)
    {
        //dodaje obiekt i przypisuje mu wagę
        objectList.Add(obj);
        propabilityList.Add(propability);
    }

    public void SetPropability(T obj, float propability)
    {
        int i = objectList.IndexOf(obj);
        propabilityList[i] = propability;
    }

    public T GetRandomObject()
    {
        // zwraca losowy obiekt
        // podczas losowania uwzględnia wagi obiektów
        float sum = SumWeights();
        float x = Random.Range(0.0f, sum);
        float counter = 0.0f;

        for(int i = 0; i < objectList.Count; i++)
        {
            counter += propabilityList[i];
            if(x <= counter)
            {
                return objectList[i];
            }
        }

        return default(T);
    }

    public void Clear()
    {
        // czyszczenie obu list 
        objectList.Clear();
        propabilityList.Clear();
    }

    float SumWeights()
    {
        float sum = 0.0f;
        for(int i = 0; i < propabilityList.Count; i++)
        {
            sum += propabilityList[i];
        }

        return sum;
    }
}
