﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : Singleton<EventManager> {

	private Dictionary <string, UnityEvent> eventDictionary = new Dictionary<string, UnityEvent>();

	public void StartListening (string eventName, UnityAction listener)
	{
		UnityEvent thisEvent = null;
		if (EventManager.Instance.eventDictionary.TryGetValue (eventName, out thisEvent))
		{
			thisEvent.AddListener (listener);
		} 
		else
		{
			thisEvent = new UnityEvent ();
			thisEvent.AddListener (listener);
			EventManager.Instance.eventDictionary.Add (eventName, thisEvent);
		}
	}

    public void StartListening(int eventName, UnityAction listener)
    {
        StartListening(eventName.ToString(), listener);
    }

    public void StopListening (string eventName, UnityAction listener)
	{
		UnityEvent thisEvent = null;
		if (EventManager.Instance.eventDictionary.TryGetValue (eventName, out thisEvent))
		{
			thisEvent.RemoveListener (listener);
		}
	}

    public void StopListening(int eventName, UnityAction listener)
    {
        StopListening(eventName.ToString(), listener);
    }


    public void TriggerEvent (string eventName)
	{
		UnityEvent thisEvent = null;
		if (EventManager.Instance.eventDictionary.TryGetValue (eventName, out thisEvent))
		{
			thisEvent.Invoke ();
		}
	}

    public void TriggerEvent(int eventName)
    {
        TriggerEvent(eventName.ToString());
    }
}
